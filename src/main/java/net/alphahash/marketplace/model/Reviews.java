package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import net.alphahash.marketplace.model.Ratings;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Reviews
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-09-02T00:30:50.006+01:00")

public class Reviews   {
  @JsonProperty("reviews")
  @Valid
  private List<String> reviews = null;

  @JsonProperty("rating_summery")
  @Valid
  private List<Ratings> ratingSummery = null;

  @JsonProperty("total_ratings")
  private BigDecimal totalRatings = null;

  public Reviews reviews(List<String> reviews) {
    this.reviews = reviews;
    return this;
  }

  public Reviews addReviewsItem(String reviewsItem) {
    if (this.reviews == null) {
      this.reviews = new ArrayList<String>();
    }
    this.reviews.add(reviewsItem);
    return this;
  }

  /**
   * Get reviews
   * @return reviews
  **/
  @ApiModelProperty(value = "")


  public List<String> getReviews() {
    return reviews;
  }

  public void setReviews(List<String> reviews) {
    this.reviews = reviews;
  }

  public Reviews ratingSummery(List<Ratings> ratingSummery) {
    this.ratingSummery = ratingSummery;
    return this;
  }

  public Reviews addRatingSummeryItem(Ratings ratingSummeryItem) {
    if (this.ratingSummery == null) {
      this.ratingSummery = new ArrayList<Ratings>();
    }
    this.ratingSummery.add(ratingSummeryItem);
    return this;
  }

  /**
   * Get ratingSummery
   * @return ratingSummery
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Ratings> getRatingSummery() {
    return ratingSummery;
  }

  public void setRatingSummery(List<Ratings> ratingSummery) {
    this.ratingSummery = ratingSummery;
  }

  public Reviews totalRatings(BigDecimal totalRatings) {
    this.totalRatings = totalRatings;
    return this;
  }

  /**
   * Get totalRatings
   * @return totalRatings
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getTotalRatings() {
    return totalRatings;
  }

  public void setTotalRatings(BigDecimal totalRatings) {
    this.totalRatings = totalRatings;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Reviews reviews = (Reviews) o;
    return Objects.equals(this.reviews, reviews.reviews) &&
        Objects.equals(this.ratingSummery, reviews.ratingSummery) &&
        Objects.equals(this.totalRatings, reviews.totalRatings);
  }

  @Override
  public int hashCode() {
    return Objects.hash(reviews, ratingSummery, totalRatings);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Reviews {\n");
    
    sb.append("    reviews: ").append(toIndentedString(reviews)).append("\n");
    sb.append("    ratingSummery: ").append(toIndentedString(ratingSummery)).append("\n");
    sb.append("    totalRatings: ").append(toIndentedString(totalRatings)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

