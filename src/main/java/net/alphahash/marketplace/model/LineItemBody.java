package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * LineItemBody
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

public class LineItemBody   {
  @JsonProperty("variant_id")
  private BigDecimal variantId = null;

  @JsonProperty("quantity")
  private BigDecimal quantity = null;

  public LineItemBody variantId(BigDecimal variantId) {
    this.variantId = variantId;
    return this;
  }

  /**
   * Get variantId
   * @return variantId
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getVariantId() {
    return variantId;
  }

  public void setVariantId(BigDecimal variantId) {
    this.variantId = variantId;
  }

  public LineItemBody quantity(BigDecimal quantity) {
    this.quantity = quantity;
    return this;
  }

  /**
   * Get quantity
   * @return quantity
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LineItemBody lineItemBody = (LineItemBody) o;
    return Objects.equals(this.variantId, lineItemBody.variantId) &&
        Objects.equals(this.quantity, lineItemBody.quantity);
  }

  @Override
  public int hashCode() {
    return Objects.hash(variantId, quantity);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LineItemBody {\n");
    
    sb.append("    variantId: ").append(toIndentedString(variantId)).append("\n");
    sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

