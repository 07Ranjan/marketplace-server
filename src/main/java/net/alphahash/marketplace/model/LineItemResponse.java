package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import net.alphahash.marketplace.model.Variant;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * LineItemResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

public class LineItemResponse   {
  @JsonProperty("id")
  private BigDecimal id = null;

  @JsonProperty("quantity")
  private BigDecimal quantity = null;

  @JsonProperty("price")
  private BigDecimal price = null;

  @JsonProperty("variant_id")
  private BigDecimal variantId = null;

  @JsonProperty("variant")
  private Variant variant = null;

  @JsonProperty("adjustments")
  @Valid
  private List<String> adjustments = null;

  @JsonProperty("single_display_amount")
  private String singleDisplayAmount = null;

  @JsonProperty("display_amount")
  private String displayAmount = null;

  @JsonProperty("total")
  private String total = null;

  public LineItemResponse id(BigDecimal id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getId() {
    return id;
  }

  public void setId(BigDecimal id) {
    this.id = id;
  }

  public LineItemResponse quantity(BigDecimal quantity) {
    this.quantity = quantity;
    return this;
  }

  /**
   * Get quantity
   * @return quantity
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public LineItemResponse price(BigDecimal price) {
    this.price = price;
    return this;
  }

  /**
   * Get price
   * @return price
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public LineItemResponse variantId(BigDecimal variantId) {
    this.variantId = variantId;
    return this;
  }

  /**
   * Get variantId
   * @return variantId
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getVariantId() {
    return variantId;
  }

  public void setVariantId(BigDecimal variantId) {
    this.variantId = variantId;
  }

  public LineItemResponse variant(Variant variant) {
    this.variant = variant;
    return this;
  }

  /**
   * Get variant
   * @return variant
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Variant getVariant() {
    return variant;
  }

  public void setVariant(Variant variant) {
    this.variant = variant;
  }

  public LineItemResponse adjustments(List<String> adjustments) {
    this.adjustments = adjustments;
    return this;
  }

  public LineItemResponse addAdjustmentsItem(String adjustmentsItem) {
    if (this.adjustments == null) {
      this.adjustments = new ArrayList<String>();
    }
    this.adjustments.add(adjustmentsItem);
    return this;
  }

  /**
   * Get adjustments
   * @return adjustments
  **/
  @ApiModelProperty(value = "")


  public List<String> getAdjustments() {
    return adjustments;
  }

  public void setAdjustments(List<String> adjustments) {
    this.adjustments = adjustments;
  }

  public LineItemResponse singleDisplayAmount(String singleDisplayAmount) {
    this.singleDisplayAmount = singleDisplayAmount;
    return this;
  }

  /**
   * Get singleDisplayAmount
   * @return singleDisplayAmount
  **/
  @ApiModelProperty(value = "")


  public String getSingleDisplayAmount() {
    return singleDisplayAmount;
  }

  public void setSingleDisplayAmount(String singleDisplayAmount) {
    this.singleDisplayAmount = singleDisplayAmount;
  }

  public LineItemResponse displayAmount(String displayAmount) {
    this.displayAmount = displayAmount;
    return this;
  }

  /**
   * Get displayAmount
   * @return displayAmount
  **/
  @ApiModelProperty(value = "")


  public String getDisplayAmount() {
    return displayAmount;
  }

  public void setDisplayAmount(String displayAmount) {
    this.displayAmount = displayAmount;
  }

  public LineItemResponse total(String total) {
    this.total = total;
    return this;
  }

  /**
   * Get total
   * @return total
  **/
  @ApiModelProperty(value = "")


  public String getTotal() {
    return total;
  }

  public void setTotal(String total) {
    this.total = total;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LineItemResponse lineItemResponse = (LineItemResponse) o;
    return Objects.equals(this.id, lineItemResponse.id) &&
        Objects.equals(this.quantity, lineItemResponse.quantity) &&
        Objects.equals(this.price, lineItemResponse.price) &&
        Objects.equals(this.variantId, lineItemResponse.variantId) &&
        Objects.equals(this.variant, lineItemResponse.variant) &&
        Objects.equals(this.adjustments, lineItemResponse.adjustments) &&
        Objects.equals(this.singleDisplayAmount, lineItemResponse.singleDisplayAmount) &&
        Objects.equals(this.displayAmount, lineItemResponse.displayAmount) &&
        Objects.equals(this.total, lineItemResponse.total);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, quantity, price, variantId, variant, adjustments, singleDisplayAmount, displayAmount, total);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LineItemResponse {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("    variantId: ").append(toIndentedString(variantId)).append("\n");
    sb.append("    variant: ").append(toIndentedString(variant)).append("\n");
    sb.append("    adjustments: ").append(toIndentedString(adjustments)).append("\n");
    sb.append("    singleDisplayAmount: ").append(toIndentedString(singleDisplayAmount)).append("\n");
    sb.append("    displayAmount: ").append(toIndentedString(displayAmount)).append("\n");
    sb.append("    total: ").append(toIndentedString(total)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

