package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import net.alphahash.marketplace.model.Taxons;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Root
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

public class Root   {
  @JsonProperty("id")
  private Long id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("pretty_name")
  private String prettyName = null;

  @JsonProperty("permalink")
  private String permalink = null;

  @JsonProperty("parent_id")
  private Long parentId = null;

  @JsonProperty("taxonomy_id")
  private Long taxonomyId = null;

  @JsonProperty("meta_title")
  private String metaTitle = null;

  @JsonProperty("meta_description")
  private String metaDescription = null;

  @JsonProperty("taxons")
  @Valid
  private List<Taxons> taxons = null;

  public Root id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Root name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Root prettyName(String prettyName) {
    this.prettyName = prettyName;
    return this;
  }

  /**
   * Get prettyName
   * @return prettyName
  **/
  @ApiModelProperty(value = "")


  public String getPrettyName() {
    return prettyName;
  }

  public void setPrettyName(String prettyName) {
    this.prettyName = prettyName;
  }

  public Root permalink(String permalink) {
    this.permalink = permalink;
    return this;
  }

  /**
   * Get permalink
   * @return permalink
  **/
  @ApiModelProperty(value = "")


  public String getPermalink() {
    return permalink;
  }

  public void setPermalink(String permalink) {
    this.permalink = permalink;
  }

  public Root parentId(Long parentId) {
    this.parentId = parentId;
    return this;
  }

  /**
   * Get parentId
   * @return parentId
  **/
  @ApiModelProperty(value = "")


  public Long getParentId() {
    return parentId;
  }

  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }

  public Root taxonomyId(Long taxonomyId) {
    this.taxonomyId = taxonomyId;
    return this;
  }

  /**
   * Get taxonomyId
   * @return taxonomyId
  **/
  @ApiModelProperty(value = "")


  public Long getTaxonomyId() {
    return taxonomyId;
  }

  public void setTaxonomyId(Long taxonomyId) {
    this.taxonomyId = taxonomyId;
  }

  public Root metaTitle(String metaTitle) {
    this.metaTitle = metaTitle;
    return this;
  }

  /**
   * Get metaTitle
   * @return metaTitle
  **/
  @ApiModelProperty(value = "")


  public String getMetaTitle() {
    return metaTitle;
  }

  public void setMetaTitle(String metaTitle) {
    this.metaTitle = metaTitle;
  }

  public Root metaDescription(String metaDescription) {
    this.metaDescription = metaDescription;
    return this;
  }

  /**
   * Get metaDescription
   * @return metaDescription
  **/
  @ApiModelProperty(value = "")


  public String getMetaDescription() {
    return metaDescription;
  }

  public void setMetaDescription(String metaDescription) {
    this.metaDescription = metaDescription;
  }

  public Root taxons(List<Taxons> taxons) {
    this.taxons = taxons;
    return this;
  }

  public Root addTaxonsItem(Taxons taxonsItem) {
    if (this.taxons == null) {
      this.taxons = new ArrayList<Taxons>();
    }
    this.taxons.add(taxonsItem);
    return this;
  }

  /**
   * Get taxons
   * @return taxons
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Taxons> getTaxons() {
    return taxons;
  }

  public void setTaxons(List<Taxons> taxons) {
    this.taxons = taxons;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Root root = (Root) o;
    return Objects.equals(this.id, root.id) &&
        Objects.equals(this.name, root.name) &&
        Objects.equals(this.prettyName, root.prettyName) &&
        Objects.equals(this.permalink, root.permalink) &&
        Objects.equals(this.parentId, root.parentId) &&
        Objects.equals(this.taxonomyId, root.taxonomyId) &&
        Objects.equals(this.metaTitle, root.metaTitle) &&
        Objects.equals(this.metaDescription, root.metaDescription) &&
        Objects.equals(this.taxons, root.taxons);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, prettyName, permalink, parentId, taxonomyId, metaTitle, metaDescription, taxons);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Root {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    prettyName: ").append(toIndentedString(prettyName)).append("\n");
    sb.append("    permalink: ").append(toIndentedString(permalink)).append("\n");
    sb.append("    parentId: ").append(toIndentedString(parentId)).append("\n");
    sb.append("    taxonomyId: ").append(toIndentedString(taxonomyId)).append("\n");
    sb.append("    metaTitle: ").append(toIndentedString(metaTitle)).append("\n");
    sb.append("    metaDescription: ").append(toIndentedString(metaDescription)).append("\n");
    sb.append("    taxons: ").append(toIndentedString(taxons)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

