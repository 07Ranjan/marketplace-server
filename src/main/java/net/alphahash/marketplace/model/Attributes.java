package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Attributes
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-09-01T13:32:21.812+01:00")

public class Attributes   {
  @JsonProperty("id")
  private BigDecimal id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("price")
  private String price = null;

  @JsonProperty("display_price")
  private String displayPrice = null;

  @JsonProperty("available_on")
  private String availableOn = null;

  @JsonProperty("slug")
  private String slug = null;

  @JsonProperty("meta_description")
  private String metaDescription = null;

  @JsonProperty("meta_keywords")
  private String metaKeywords = null;

  @JsonProperty("shipping_category_id")
  private BigDecimal shippingCategoryId = null;

  @JsonProperty("taxon_ids")
  @Valid
  private List<BigDecimal> taxonIds = null;

  @JsonProperty("total_on_hand")
  private BigDecimal totalOnHand = null;

  @JsonProperty("avg_rating")
  private String avgRating = null;

  @JsonProperty("reviews_count")
  private String reviewsCount = null;

  @JsonProperty("currency")
  private String currency = null;

  @JsonProperty("currency_symbol")
  private String currencySymbol = null;

  @JsonProperty("has_variants")
  private Boolean hasVariants = null;

  @JsonProperty("cost_price")
  private String costPrice = null;

  @JsonProperty("is_favorited_by_current_user")
  private Boolean isFavoritedByCurrentUser = null;

  public Attributes id(BigDecimal id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getId() {
    return id;
  }

  public void setId(BigDecimal id) {
    this.id = id;
  }

  public Attributes name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Attributes description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(value = "")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Attributes price(String price) {
    this.price = price;
    return this;
  }

  /**
   * Get price
   * @return price
  **/
  @ApiModelProperty(value = "")


  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public Attributes displayPrice(String displayPrice) {
    this.displayPrice = displayPrice;
    return this;
  }

  /**
   * Get displayPrice
   * @return displayPrice
  **/
  @ApiModelProperty(value = "")


  public String getDisplayPrice() {
    return displayPrice;
  }

  public void setDisplayPrice(String displayPrice) {
    this.displayPrice = displayPrice;
  }

  public Attributes availableOn(String availableOn) {
    this.availableOn = availableOn;
    return this;
  }

  /**
   * Get availableOn
   * @return availableOn
  **/
  @ApiModelProperty(value = "")


  public String getAvailableOn() {
    return availableOn;
  }

  public void setAvailableOn(String availableOn) {
    this.availableOn = availableOn;
  }

  public Attributes slug(String slug) {
    this.slug = slug;
    return this;
  }

  /**
   * Get slug
   * @return slug
  **/
  @ApiModelProperty(value = "")


  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

  public Attributes metaDescription(String metaDescription) {
    this.metaDescription = metaDescription;
    return this;
  }

  /**
   * Get metaDescription
   * @return metaDescription
  **/
  @ApiModelProperty(value = "")


  public String getMetaDescription() {
    return metaDescription;
  }

  public void setMetaDescription(String metaDescription) {
    this.metaDescription = metaDescription;
  }

  public Attributes metaKeywords(String metaKeywords) {
    this.metaKeywords = metaKeywords;
    return this;
  }

  /**
   * Get metaKeywords
   * @return metaKeywords
  **/
  @ApiModelProperty(value = "")


  public String getMetaKeywords() {
    return metaKeywords;
  }

  public void setMetaKeywords(String metaKeywords) {
    this.metaKeywords = metaKeywords;
  }

  public Attributes shippingCategoryId(BigDecimal shippingCategoryId) {
    this.shippingCategoryId = shippingCategoryId;
    return this;
  }

  /**
   * Get shippingCategoryId
   * @return shippingCategoryId
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getShippingCategoryId() {
    return shippingCategoryId;
  }

  public void setShippingCategoryId(BigDecimal shippingCategoryId) {
    this.shippingCategoryId = shippingCategoryId;
  }

  public Attributes taxonIds(List<BigDecimal> taxonIds) {
    this.taxonIds = taxonIds;
    return this;
  }

  public Attributes addTaxonIdsItem(BigDecimal taxonIdsItem) {
    if (this.taxonIds == null) {
      this.taxonIds = new ArrayList<BigDecimal>();
    }
    this.taxonIds.add(taxonIdsItem);
    return this;
  }

  /**
   * Get taxonIds
   * @return taxonIds
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<BigDecimal> getTaxonIds() {
    return taxonIds;
  }

  public void setTaxonIds(List<BigDecimal> taxonIds) {
    this.taxonIds = taxonIds;
  }

  public Attributes totalOnHand(BigDecimal totalOnHand) {
    this.totalOnHand = totalOnHand;
    return this;
  }

  /**
   * Get totalOnHand
   * @return totalOnHand
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getTotalOnHand() {
    return totalOnHand;
  }

  public void setTotalOnHand(BigDecimal totalOnHand) {
    this.totalOnHand = totalOnHand;
  }

  public Attributes avgRating(String avgRating) {
    this.avgRating = avgRating;
    return this;
  }

  /**
   * Get avgRating
   * @return avgRating
  **/
  @ApiModelProperty(value = "")


  public String getAvgRating() {
    return avgRating;
  }

  public void setAvgRating(String avgRating) {
    this.avgRating = avgRating;
  }

  public Attributes reviewsCount(String reviewsCount) {
    this.reviewsCount = reviewsCount;
    return this;
  }

  /**
   * Get reviewsCount
   * @return reviewsCount
  **/
  @ApiModelProperty(value = "")


  public String getReviewsCount() {
    return reviewsCount;
  }

  public void setReviewsCount(String reviewsCount) {
    this.reviewsCount = reviewsCount;
  }

  public Attributes currency(String currency) {
    this.currency = currency;
    return this;
  }

  /**
   * Get currency
   * @return currency
  **/
  @ApiModelProperty(value = "")


  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public Attributes currencySymbol(String currencySymbol) {
    this.currencySymbol = currencySymbol;
    return this;
  }

  /**
   * Get currencySymbol
   * @return currencySymbol
  **/
  @ApiModelProperty(value = "")


  public String getCurrencySymbol() {
    return currencySymbol;
  }

  public void setCurrencySymbol(String currencySymbol) {
    this.currencySymbol = currencySymbol;
  }

  public Attributes hasVariants(Boolean hasVariants) {
    this.hasVariants = hasVariants;
    return this;
  }

  /**
   * Get hasVariants
   * @return hasVariants
  **/
  @ApiModelProperty(value = "")


  public Boolean isHasVariants() {
    return hasVariants;
  }

  public void setHasVariants(Boolean hasVariants) {
    this.hasVariants = hasVariants;
  }

  public Attributes costPrice(String costPrice) {
    this.costPrice = costPrice;
    return this;
  }

  /**
   * Get costPrice
   * @return costPrice
  **/
  @ApiModelProperty(value = "")


  public String getCostPrice() {
    return costPrice;
  }

  public void setCostPrice(String costPrice) {
    this.costPrice = costPrice;
  }

  public Attributes isFavoritedByCurrentUser(Boolean isFavoritedByCurrentUser) {
    this.isFavoritedByCurrentUser = isFavoritedByCurrentUser;
    return this;
  }

  /**
   * Get isFavoritedByCurrentUser
   * @return isFavoritedByCurrentUser
  **/
  @ApiModelProperty(value = "")


  public Boolean isIsFavoritedByCurrentUser() {
    return isFavoritedByCurrentUser;
  }

  public void setIsFavoritedByCurrentUser(Boolean isFavoritedByCurrentUser) {
    this.isFavoritedByCurrentUser = isFavoritedByCurrentUser;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Attributes attributes = (Attributes) o;
    return Objects.equals(this.id, attributes.id) &&
        Objects.equals(this.name, attributes.name) &&
        Objects.equals(this.description, attributes.description) &&
        Objects.equals(this.price, attributes.price) &&
        Objects.equals(this.displayPrice, attributes.displayPrice) &&
        Objects.equals(this.availableOn, attributes.availableOn) &&
        Objects.equals(this.slug, attributes.slug) &&
        Objects.equals(this.metaDescription, attributes.metaDescription) &&
        Objects.equals(this.metaKeywords, attributes.metaKeywords) &&
        Objects.equals(this.shippingCategoryId, attributes.shippingCategoryId) &&
        Objects.equals(this.taxonIds, attributes.taxonIds) &&
        Objects.equals(this.totalOnHand, attributes.totalOnHand) &&
        Objects.equals(this.avgRating, attributes.avgRating) &&
        Objects.equals(this.reviewsCount, attributes.reviewsCount) &&
        Objects.equals(this.currency, attributes.currency) &&
        Objects.equals(this.currencySymbol, attributes.currencySymbol) &&
        Objects.equals(this.hasVariants, attributes.hasVariants) &&
        Objects.equals(this.costPrice, attributes.costPrice) &&
        Objects.equals(this.isFavoritedByCurrentUser, attributes.isFavoritedByCurrentUser);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, description, price, displayPrice, availableOn, slug, metaDescription, metaKeywords, shippingCategoryId, taxonIds, totalOnHand, avgRating, reviewsCount, currency, currencySymbol, hasVariants, costPrice, isFavoritedByCurrentUser);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Attributes {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("    displayPrice: ").append(toIndentedString(displayPrice)).append("\n");
    sb.append("    availableOn: ").append(toIndentedString(availableOn)).append("\n");
    sb.append("    slug: ").append(toIndentedString(slug)).append("\n");
    sb.append("    metaDescription: ").append(toIndentedString(metaDescription)).append("\n");
    sb.append("    metaKeywords: ").append(toIndentedString(metaKeywords)).append("\n");
    sb.append("    shippingCategoryId: ").append(toIndentedString(shippingCategoryId)).append("\n");
    sb.append("    taxonIds: ").append(toIndentedString(taxonIds)).append("\n");
    sb.append("    totalOnHand: ").append(toIndentedString(totalOnHand)).append("\n");
    sb.append("    avgRating: ").append(toIndentedString(avgRating)).append("\n");
    sb.append("    reviewsCount: ").append(toIndentedString(reviewsCount)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("    currencySymbol: ").append(toIndentedString(currencySymbol)).append("\n");
    sb.append("    hasVariants: ").append(toIndentedString(hasVariants)).append("\n");
    sb.append("    costPrice: ").append(toIndentedString(costPrice)).append("\n");
    sb.append("    isFavoritedByCurrentUser: ").append(toIndentedString(isFavoritedByCurrentUser)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

