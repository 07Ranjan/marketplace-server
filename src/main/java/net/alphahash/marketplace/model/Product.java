package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import net.alphahash.marketplace.model.Classification;
import net.alphahash.marketplace.model.OptionType;
import net.alphahash.marketplace.model.ProductProperty;
import net.alphahash.marketplace.model.Variant;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Product
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

public class Product   {
  @JsonProperty("id")
  private BigDecimal id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("price")
  private String price = null;

  @JsonProperty("display_price")
  private String displayPrice = null;

  @JsonProperty("available_on")
  private String availableOn = null;

  @JsonProperty("slug")
  private String slug = null;

  @JsonProperty("is_favorited_by_current_user")
  private Boolean isFavoritedByCurrentUser = null;

  @JsonProperty("meta_description")
  private String metaDescription = null;

  @JsonProperty("meta_keywords")
  private String metaKeywords = null;

  @JsonProperty("shipping_category_id")
  private BigDecimal shippingCategoryId = null;

  @JsonProperty("taxon_ids")
  @Valid
  private List<BigDecimal> taxonIds = null;

  @JsonProperty("total_on_hand")
  private BigDecimal totalOnHand = null;

  @JsonProperty("has_variants")
  private Boolean hasVariants = null;

  @JsonProperty("master")
  private Variant master = null;

  @JsonProperty("variants")
  @Valid
  private List<Variant> variants = null;

  @JsonProperty("option_types")
  @Valid
  private List<OptionType> optionTypes = null;

  @JsonProperty("product_properties")
  private ProductProperty productProperties = null;

  @JsonProperty("classifications")
  private Classification classifications = null;

  @JsonProperty("avg_rating")
  private BigDecimal avgRating = null;

  @JsonProperty("reviews_count")
  private BigDecimal reviewsCount = null;

  @JsonProperty("product_url?")
  private String productUrl = null;

  public Product id(BigDecimal id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getId() {
    return id;
  }

  public void setId(BigDecimal id) {
    this.id = id;
  }

  public Product name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Product description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(value = "")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Product price(String price) {
    this.price = price;
    return this;
  }

  /**
   * Get price
   * @return price
  **/
  @ApiModelProperty(value = "")


  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public Product displayPrice(String displayPrice) {
    this.displayPrice = displayPrice;
    return this;
  }

  /**
   * Get displayPrice
   * @return displayPrice
  **/
  @ApiModelProperty(value = "")


  public String getDisplayPrice() {
    return displayPrice;
  }

  public void setDisplayPrice(String displayPrice) {
    this.displayPrice = displayPrice;
  }

  public Product availableOn(String availableOn) {
    this.availableOn = availableOn;
    return this;
  }

  /**
   * Get availableOn
   * @return availableOn
  **/
  @ApiModelProperty(value = "")


  public String getAvailableOn() {
    return availableOn;
  }

  public void setAvailableOn(String availableOn) {
    this.availableOn = availableOn;
  }

  public Product slug(String slug) {
    this.slug = slug;
    return this;
  }

  /**
   * Get slug
   * @return slug
  **/
  @ApiModelProperty(value = "")


  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

  public Product isFavoritedByCurrentUser(Boolean isFavoritedByCurrentUser) {
    this.isFavoritedByCurrentUser = isFavoritedByCurrentUser;
    return this;
  }

  /**
   * Get isFavoritedByCurrentUser
   * @return isFavoritedByCurrentUser
  **/
  @ApiModelProperty(value = "")


  public Boolean isIsFavoritedByCurrentUser() {
    return isFavoritedByCurrentUser;
  }

  public void setIsFavoritedByCurrentUser(Boolean isFavoritedByCurrentUser) {
    this.isFavoritedByCurrentUser = isFavoritedByCurrentUser;
  }

  public Product metaDescription(String metaDescription) {
    this.metaDescription = metaDescription;
    return this;
  }

  /**
   * Get metaDescription
   * @return metaDescription
  **/
  @ApiModelProperty(value = "")


  public String getMetaDescription() {
    return metaDescription;
  }

  public void setMetaDescription(String metaDescription) {
    this.metaDescription = metaDescription;
  }

  public Product metaKeywords(String metaKeywords) {
    this.metaKeywords = metaKeywords;
    return this;
  }

  /**
   * Get metaKeywords
   * @return metaKeywords
  **/
  @ApiModelProperty(value = "")


  public String getMetaKeywords() {
    return metaKeywords;
  }

  public void setMetaKeywords(String metaKeywords) {
    this.metaKeywords = metaKeywords;
  }

  public Product shippingCategoryId(BigDecimal shippingCategoryId) {
    this.shippingCategoryId = shippingCategoryId;
    return this;
  }

  /**
   * Get shippingCategoryId
   * @return shippingCategoryId
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getShippingCategoryId() {
    return shippingCategoryId;
  }

  public void setShippingCategoryId(BigDecimal shippingCategoryId) {
    this.shippingCategoryId = shippingCategoryId;
  }

  public Product taxonIds(List<BigDecimal> taxonIds) {
    this.taxonIds = taxonIds;
    return this;
  }

  public Product addTaxonIdsItem(BigDecimal taxonIdsItem) {
    if (this.taxonIds == null) {
      this.taxonIds = new ArrayList<BigDecimal>();
    }
    this.taxonIds.add(taxonIdsItem);
    return this;
  }

  /**
   * Get taxonIds
   * @return taxonIds
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<BigDecimal> getTaxonIds() {
    return taxonIds;
  }

  public void setTaxonIds(List<BigDecimal> taxonIds) {
    this.taxonIds = taxonIds;
  }

  public Product totalOnHand(BigDecimal totalOnHand) {
    this.totalOnHand = totalOnHand;
    return this;
  }

  /**
   * Get totalOnHand
   * @return totalOnHand
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getTotalOnHand() {
    return totalOnHand;
  }

  public void setTotalOnHand(BigDecimal totalOnHand) {
    this.totalOnHand = totalOnHand;
  }

  public Product hasVariants(Boolean hasVariants) {
    this.hasVariants = hasVariants;
    return this;
  }

  /**
   * Get hasVariants
   * @return hasVariants
  **/
  @ApiModelProperty(value = "")


  public Boolean isHasVariants() {
    return hasVariants;
  }

  public void setHasVariants(Boolean hasVariants) {
    this.hasVariants = hasVariants;
  }

  public Product master(Variant master) {
    this.master = master;
    return this;
  }

  /**
   * Get master
   * @return master
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Variant getMaster() {
    return master;
  }

  public void setMaster(Variant master) {
    this.master = master;
  }

  public Product variants(List<Variant> variants) {
    this.variants = variants;
    return this;
  }

  public Product addVariantsItem(Variant variantsItem) {
    if (this.variants == null) {
      this.variants = new ArrayList<Variant>();
    }
    this.variants.add(variantsItem);
    return this;
  }

  /**
   * Get variants
   * @return variants
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Variant> getVariants() {
    return variants;
  }

  public void setVariants(List<Variant> variants) {
    this.variants = variants;
  }

  public Product optionTypes(List<OptionType> optionTypes) {
    this.optionTypes = optionTypes;
    return this;
  }

  public Product addOptionTypesItem(OptionType optionTypesItem) {
    if (this.optionTypes == null) {
      this.optionTypes = new ArrayList<OptionType>();
    }
    this.optionTypes.add(optionTypesItem);
    return this;
  }

  /**
   * Get optionTypes
   * @return optionTypes
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<OptionType> getOptionTypes() {
    return optionTypes;
  }

  public void setOptionTypes(List<OptionType> optionTypes) {
    this.optionTypes = optionTypes;
  }

  public Product productProperties(ProductProperty productProperties) {
    this.productProperties = productProperties;
    return this;
  }

  /**
   * Get productProperties
   * @return productProperties
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ProductProperty getProductProperties() {
    return productProperties;
  }

  public void setProductProperties(ProductProperty productProperties) {
    this.productProperties = productProperties;
  }

  public Product classifications(Classification classifications) {
    this.classifications = classifications;
    return this;
  }

  /**
   * Get classifications
   * @return classifications
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Classification getClassifications() {
    return classifications;
  }

  public void setClassifications(Classification classifications) {
    this.classifications = classifications;
  }

  public Product avgRating(BigDecimal avgRating) {
    this.avgRating = avgRating;
    return this;
  }

  /**
   * Get avgRating
   * @return avgRating
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getAvgRating() {
    return avgRating;
  }

  public void setAvgRating(BigDecimal avgRating) {
    this.avgRating = avgRating;
  }

  public Product reviewsCount(BigDecimal reviewsCount) {
    this.reviewsCount = reviewsCount;
    return this;
  }

  /**
   * Get reviewsCount
   * @return reviewsCount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getReviewsCount() {
    return reviewsCount;
  }

  public void setReviewsCount(BigDecimal reviewsCount) {
    this.reviewsCount = reviewsCount;
  }

  public Product productUrl(String productUrl) {
    this.productUrl = productUrl;
    return this;
  }

  /**
   * Get productUrl
   * @return productUrl
  **/
  @ApiModelProperty(value = "")


  public String getProductUrl() {
    return productUrl;
  }

  public void setProductUrl(String productUrl) {
    this.productUrl = productUrl;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Product product = (Product) o;
    return Objects.equals(this.id, product.id) &&
        Objects.equals(this.name, product.name) &&
        Objects.equals(this.description, product.description) &&
        Objects.equals(this.price, product.price) &&
        Objects.equals(this.displayPrice, product.displayPrice) &&
        Objects.equals(this.availableOn, product.availableOn) &&
        Objects.equals(this.slug, product.slug) &&
        Objects.equals(this.isFavoritedByCurrentUser, product.isFavoritedByCurrentUser) &&
        Objects.equals(this.metaDescription, product.metaDescription) &&
        Objects.equals(this.metaKeywords, product.metaKeywords) &&
        Objects.equals(this.shippingCategoryId, product.shippingCategoryId) &&
        Objects.equals(this.taxonIds, product.taxonIds) &&
        Objects.equals(this.totalOnHand, product.totalOnHand) &&
        Objects.equals(this.hasVariants, product.hasVariants) &&
        Objects.equals(this.master, product.master) &&
        Objects.equals(this.variants, product.variants) &&
        Objects.equals(this.optionTypes, product.optionTypes) &&
        Objects.equals(this.productProperties, product.productProperties) &&
        Objects.equals(this.classifications, product.classifications) &&
        Objects.equals(this.avgRating, product.avgRating) &&
        Objects.equals(this.reviewsCount, product.reviewsCount) &&
        Objects.equals(this.productUrl, product.productUrl);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, description, price, displayPrice, availableOn, slug, isFavoritedByCurrentUser, metaDescription, metaKeywords, shippingCategoryId, taxonIds, totalOnHand, hasVariants, master, variants, optionTypes, productProperties, classifications, avgRating, reviewsCount, productUrl);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Product {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("    displayPrice: ").append(toIndentedString(displayPrice)).append("\n");
    sb.append("    availableOn: ").append(toIndentedString(availableOn)).append("\n");
    sb.append("    slug: ").append(toIndentedString(slug)).append("\n");
    sb.append("    isFavoritedByCurrentUser: ").append(toIndentedString(isFavoritedByCurrentUser)).append("\n");
    sb.append("    metaDescription: ").append(toIndentedString(metaDescription)).append("\n");
    sb.append("    metaKeywords: ").append(toIndentedString(metaKeywords)).append("\n");
    sb.append("    shippingCategoryId: ").append(toIndentedString(shippingCategoryId)).append("\n");
    sb.append("    taxonIds: ").append(toIndentedString(taxonIds)).append("\n");
    sb.append("    totalOnHand: ").append(toIndentedString(totalOnHand)).append("\n");
    sb.append("    hasVariants: ").append(toIndentedString(hasVariants)).append("\n");
    sb.append("    master: ").append(toIndentedString(master)).append("\n");
    sb.append("    variants: ").append(toIndentedString(variants)).append("\n");
    sb.append("    optionTypes: ").append(toIndentedString(optionTypes)).append("\n");
    sb.append("    productProperties: ").append(toIndentedString(productProperties)).append("\n");
    sb.append("    classifications: ").append(toIndentedString(classifications)).append("\n");
    sb.append("    avgRating: ").append(toIndentedString(avgRating)).append("\n");
    sb.append("    reviewsCount: ").append(toIndentedString(reviewsCount)).append("\n");
    sb.append("    productUrl: ").append(toIndentedString(productUrl)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

