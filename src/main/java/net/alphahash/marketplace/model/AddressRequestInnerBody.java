package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.alphahash.marketplace.model.UiAddress;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * AddressRequestInnerBody
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

public class AddressRequestInnerBody   {
  @JsonProperty("email")
  private String email = null;

  @JsonProperty("bill_address_attributes")
  private UiAddress billAddressAttributes = null;

  @JsonProperty("ship_address_attributes")
  private UiAddress shipAddressAttributes = null;

  public AddressRequestInnerBody email(String email) {
    this.email = email;
    return this;
  }

  /**
   * Get email
   * @return email
  **/
  @ApiModelProperty(value = "")


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public AddressRequestInnerBody billAddressAttributes(UiAddress billAddressAttributes) {
    this.billAddressAttributes = billAddressAttributes;
    return this;
  }

  /**
   * Get billAddressAttributes
   * @return billAddressAttributes
  **/
  @ApiModelProperty(value = "")

  @Valid

  public UiAddress getBillAddressAttributes() {
    return billAddressAttributes;
  }

  public void setBillAddressAttributes(UiAddress billAddressAttributes) {
    this.billAddressAttributes = billAddressAttributes;
  }

  public AddressRequestInnerBody shipAddressAttributes(UiAddress shipAddressAttributes) {
    this.shipAddressAttributes = shipAddressAttributes;
    return this;
  }

  /**
   * Get shipAddressAttributes
   * @return shipAddressAttributes
  **/
  @ApiModelProperty(value = "")

  @Valid

  public UiAddress getShipAddressAttributes() {
    return shipAddressAttributes;
  }

  public void setShipAddressAttributes(UiAddress shipAddressAttributes) {
    this.shipAddressAttributes = shipAddressAttributes;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AddressRequestInnerBody addressRequestInnerBody = (AddressRequestInnerBody) o;
    return Objects.equals(this.email, addressRequestInnerBody.email) &&
        Objects.equals(this.billAddressAttributes, addressRequestInnerBody.billAddressAttributes) &&
        Objects.equals(this.shipAddressAttributes, addressRequestInnerBody.shipAddressAttributes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(email, billAddressAttributes, shipAddressAttributes);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AddressRequestInnerBody {\n");
    
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    billAddressAttributes: ").append(toIndentedString(billAddressAttributes)).append("\n");
    sb.append("    shipAddressAttributes: ").append(toIndentedString(shipAddressAttributes)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

