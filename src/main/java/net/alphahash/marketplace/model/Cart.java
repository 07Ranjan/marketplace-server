package net.alphahash.marketplace.model;

public class Cart {

    private static Cart instance = null;

    private  Boolean itemPresent = false;
    private  int itemNumber = 0;
    private Cart() {}


    public static Cart getInstance(){
        if (instance == null) {
            instance = new Cart();
        }
        return instance;
    }

    public Boolean getItemPresent() {
        return itemPresent;
    }

    public void setItemPresent(Boolean itemPresent) {
        this.itemPresent = itemPresent;
    }

    public int getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(int itemNumber) {
        this.itemNumber = itemNumber;
    }
}
