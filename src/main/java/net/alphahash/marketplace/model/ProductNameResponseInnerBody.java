package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import net.alphahash.marketplace.model.Attributes;
import net.alphahash.marketplace.model.Included;
import net.alphahash.marketplace.model.Relationships;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ProductNameResponseInnerBody
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-09-01T13:32:21.812+01:00")

public class ProductNameResponseInnerBody   {
  @JsonProperty("id")
  private BigDecimal id = null;

  @JsonProperty("type")
  private String type = null;

  @JsonProperty("attributes")
  private Attributes attributes = null;

  @JsonProperty("relationships")
  private Relationships relationships = null;

  @JsonProperty("included")
  private Included included = null;

  public ProductNameResponseInnerBody id(BigDecimal id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getId() {
    return id;
  }

  public void setId(BigDecimal id) {
    this.id = id;
  }

  public ProductNameResponseInnerBody type(String type) {
    this.type = type;
    return this;
  }

  /**
   * Get type
   * @return type
  **/
  @ApiModelProperty(value = "")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public ProductNameResponseInnerBody attributes(Attributes attributes) {
    this.attributes = attributes;
    return this;
  }

  /**
   * Get attributes
   * @return attributes
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Attributes getAttributes() {
    return attributes;
  }

  public void setAttributes(Attributes attributes) {
    this.attributes = attributes;
  }

  public ProductNameResponseInnerBody relationships(Relationships relationships) {
    this.relationships = relationships;
    return this;
  }

  /**
   * Get relationships
   * @return relationships
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Relationships getRelationships() {
    return relationships;
  }

  public void setRelationships(Relationships relationships) {
    this.relationships = relationships;
  }

  public ProductNameResponseInnerBody included(Included included) {
    this.included = included;
    return this;
  }

  /**
   * Get included
   * @return included
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Included getIncluded() {
    return included;
  }

  public void setIncluded(Included included) {
    this.included = included;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProductNameResponseInnerBody productNameResponseInnerBody = (ProductNameResponseInnerBody) o;
    return Objects.equals(this.id, productNameResponseInnerBody.id) &&
        Objects.equals(this.type, productNameResponseInnerBody.type) &&
        Objects.equals(this.attributes, productNameResponseInnerBody.attributes) &&
        Objects.equals(this.relationships, productNameResponseInnerBody.relationships) &&
        Objects.equals(this.included, productNameResponseInnerBody.included);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, type, attributes, relationships, included);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProductNameResponseInnerBody {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    attributes: ").append(toIndentedString(attributes)).append("\n");
    sb.append("    relationships: ").append(toIndentedString(relationships)).append("\n");
    sb.append("    included: ").append(toIndentedString(included)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

