package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import net.alphahash.marketplace.model.Classifications;
import net.alphahash.marketplace.model.Master;
import net.alphahash.marketplace.model.OptionTypeAttributes;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Included
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-09-01T13:32:21.812+01:00")

public class Included   {
  @JsonProperty("master")
  private Master master = null;

  @JsonProperty("variants")
  @Valid
  private List<String> variants = null;

  @JsonProperty("option_types")
  @Valid
  private List<OptionTypeAttributes> optionTypes = null;

  @JsonProperty("product_properties")
  @Valid
  private List<String> productProperties = null;

  @JsonProperty("classifications")
  @Valid
  private List<Classifications> classifications = null;

  public Included master(Master master) {
    this.master = master;
    return this;
  }

  /**
   * Get master
   * @return master
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Master getMaster() {
    return master;
  }

  public void setMaster(Master master) {
    this.master = master;
  }

  public Included variants(List<String> variants) {
    this.variants = variants;
    return this;
  }

  public Included addVariantsItem(String variantsItem) {
    if (this.variants == null) {
      this.variants = new ArrayList<String>();
    }
    this.variants.add(variantsItem);
    return this;
  }

  /**
   * Get variants
   * @return variants
  **/
  @ApiModelProperty(value = "")


  public List<String> getVariants() {
    return variants;
  }

  public void setVariants(List<String> variants) {
    this.variants = variants;
  }

  public Included optionTypes(List<OptionTypeAttributes> optionTypes) {
    this.optionTypes = optionTypes;
    return this;
  }

  public Included addOptionTypesItem(OptionTypeAttributes optionTypesItem) {
    if (this.optionTypes == null) {
      this.optionTypes = new ArrayList<OptionTypeAttributes>();
    }
    this.optionTypes.add(optionTypesItem);
    return this;
  }

  /**
   * Get optionTypes
   * @return optionTypes
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<OptionTypeAttributes> getOptionTypes() {
    return optionTypes;
  }

  public void setOptionTypes(List<OptionTypeAttributes> optionTypes) {
    this.optionTypes = optionTypes;
  }

  public Included productProperties(List<String> productProperties) {
    this.productProperties = productProperties;
    return this;
  }

  public Included addProductPropertiesItem(String productPropertiesItem) {
    if (this.productProperties == null) {
      this.productProperties = new ArrayList<String>();
    }
    this.productProperties.add(productPropertiesItem);
    return this;
  }

  /**
   * Get productProperties
   * @return productProperties
  **/
  @ApiModelProperty(value = "")


  public List<String> getProductProperties() {
    return productProperties;
  }

  public void setProductProperties(List<String> productProperties) {
    this.productProperties = productProperties;
  }

  public Included classifications(List<Classifications> classifications) {
    this.classifications = classifications;
    return this;
  }

  public Included addClassificationsItem(Classifications classificationsItem) {
    if (this.classifications == null) {
      this.classifications = new ArrayList<Classifications>();
    }
    this.classifications.add(classificationsItem);
    return this;
  }

  /**
   * Get classifications
   * @return classifications
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Classifications> getClassifications() {
    return classifications;
  }

  public void setClassifications(List<Classifications> classifications) {
    this.classifications = classifications;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Included included = (Included) o;
    return Objects.equals(this.master, included.master) &&
        Objects.equals(this.variants, included.variants) &&
        Objects.equals(this.optionTypes, included.optionTypes) &&
        Objects.equals(this.productProperties, included.productProperties) &&
        Objects.equals(this.classifications, included.classifications);
  }

  @Override
  public int hashCode() {
    return Objects.hash(master, variants, optionTypes, productProperties, classifications);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Included {\n");
    
    sb.append("    master: ").append(toIndentedString(master)).append("\n");
    sb.append("    variants: ").append(toIndentedString(variants)).append("\n");
    sb.append("    optionTypes: ").append(toIndentedString(optionTypes)).append("\n");
    sb.append("    productProperties: ").append(toIndentedString(productProperties)).append("\n");
    sb.append("    classifications: ").append(toIndentedString(classifications)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

