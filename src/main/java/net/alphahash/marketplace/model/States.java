package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import net.alphahash.marketplace.model.StateDetails;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * States
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

public class States   {
  @JsonProperty("states")
  @Valid
  private List<StateDetails> states = null;

  public States states(List<StateDetails> states) {
    this.states = states;
    return this;
  }

  public States addStatesItem(StateDetails statesItem) {
    if (this.states == null) {
      this.states = new ArrayList<StateDetails>();
    }
    this.states.add(statesItem);
    return this;
  }

  /**
   * Get states
   * @return states
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<StateDetails> getStates() {
    return states;
  }

  public void setStates(List<StateDetails> states) {
    this.states = states;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    States states = (States) o;
    return Objects.equals(this.states, states.states);
  }

  @Override
  public int hashCode() {
    return Objects.hash(states);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class States {\n");
    
    sb.append("    states: ").append(toIndentedString(states)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

