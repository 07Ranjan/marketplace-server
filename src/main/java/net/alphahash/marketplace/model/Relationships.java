package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.alphahash.marketplace.model.Data;
import net.alphahash.marketplace.model.DataArray;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Relationships
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-09-01T13:32:21.812+01:00")

public class Relationships   {
  @JsonProperty("master")
  private Data master = null;

  @JsonProperty("variants")
  private DataArray variants = null;

  @JsonProperty("option_types")
  private DataArray optionTypes = null;

  @JsonProperty("product_properties")
  private DataArray productProperties = null;

  @JsonProperty("classifications")
  private DataArray classifications = null;

  public Relationships master(Data master) {
    this.master = master;
    return this;
  }

  /**
   * Get master
   * @return master
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Data getMaster() {
    return master;
  }

  public void setMaster(Data master) {
    this.master = master;
  }

  public Relationships variants(DataArray variants) {
    this.variants = variants;
    return this;
  }

  /**
   * Get variants
   * @return variants
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DataArray getVariants() {
    return variants;
  }

  public void setVariants(DataArray variants) {
    this.variants = variants;
  }

  public Relationships optionTypes(DataArray optionTypes) {
    this.optionTypes = optionTypes;
    return this;
  }

  /**
   * Get optionTypes
   * @return optionTypes
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DataArray getOptionTypes() {
    return optionTypes;
  }

  public void setOptionTypes(DataArray optionTypes) {
    this.optionTypes = optionTypes;
  }

  public Relationships productProperties(DataArray productProperties) {
    this.productProperties = productProperties;
    return this;
  }

  /**
   * Get productProperties
   * @return productProperties
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DataArray getProductProperties() {
    return productProperties;
  }

  public void setProductProperties(DataArray productProperties) {
    this.productProperties = productProperties;
  }

  public Relationships classifications(DataArray classifications) {
    this.classifications = classifications;
    return this;
  }

  /**
   * Get classifications
   * @return classifications
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DataArray getClassifications() {
    return classifications;
  }

  public void setClassifications(DataArray classifications) {
    this.classifications = classifications;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Relationships relationships = (Relationships) o;
    return Objects.equals(this.master, relationships.master) &&
        Objects.equals(this.variants, relationships.variants) &&
        Objects.equals(this.optionTypes, relationships.optionTypes) &&
        Objects.equals(this.productProperties, relationships.productProperties) &&
        Objects.equals(this.classifications, relationships.classifications);
  }

  @Override
  public int hashCode() {
    return Objects.hash(master, variants, optionTypes, productProperties, classifications);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Relationships {\n");
    
    sb.append("    master: ").append(toIndentedString(master)).append("\n");
    sb.append("    variants: ").append(toIndentedString(variants)).append("\n");
    sb.append("    optionTypes: ").append(toIndentedString(optionTypes)).append("\n");
    sb.append("    productProperties: ").append(toIndentedString(productProperties)).append("\n");
    sb.append("    classifications: ").append(toIndentedString(classifications)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

