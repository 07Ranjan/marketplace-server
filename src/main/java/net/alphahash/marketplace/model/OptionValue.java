package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * OptionValue
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

public class OptionValue   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("presentation")
  private String presentation = null;

  @JsonProperty("option_type_name")
  private String optionTypeName = null;

  @JsonProperty("option_type_id")
  private BigDecimal optionTypeId = null;

  @JsonProperty("option_type_presentation")
  private String optionTypePresentation = null;

  public OptionValue id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public OptionValue name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public OptionValue presentation(String presentation) {
    this.presentation = presentation;
    return this;
  }

  /**
   * Get presentation
   * @return presentation
  **/
  @ApiModelProperty(value = "")


  public String getPresentation() {
    return presentation;
  }

  public void setPresentation(String presentation) {
    this.presentation = presentation;
  }

  public OptionValue optionTypeName(String optionTypeName) {
    this.optionTypeName = optionTypeName;
    return this;
  }

  /**
   * Get optionTypeName
   * @return optionTypeName
  **/
  @ApiModelProperty(value = "")


  public String getOptionTypeName() {
    return optionTypeName;
  }

  public void setOptionTypeName(String optionTypeName) {
    this.optionTypeName = optionTypeName;
  }

  public OptionValue optionTypeId(BigDecimal optionTypeId) {
    this.optionTypeId = optionTypeId;
    return this;
  }

  /**
   * Get optionTypeId
   * @return optionTypeId
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getOptionTypeId() {
    return optionTypeId;
  }

  public void setOptionTypeId(BigDecimal optionTypeId) {
    this.optionTypeId = optionTypeId;
  }

  public OptionValue optionTypePresentation(String optionTypePresentation) {
    this.optionTypePresentation = optionTypePresentation;
    return this;
  }

  /**
   * Get optionTypePresentation
   * @return optionTypePresentation
  **/
  @ApiModelProperty(value = "")


  public String getOptionTypePresentation() {
    return optionTypePresentation;
  }

  public void setOptionTypePresentation(String optionTypePresentation) {
    this.optionTypePresentation = optionTypePresentation;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OptionValue optionValue = (OptionValue) o;
    return Objects.equals(this.id, optionValue.id) &&
        Objects.equals(this.name, optionValue.name) &&
        Objects.equals(this.presentation, optionValue.presentation) &&
        Objects.equals(this.optionTypeName, optionValue.optionTypeName) &&
        Objects.equals(this.optionTypeId, optionValue.optionTypeId) &&
        Objects.equals(this.optionTypePresentation, optionValue.optionTypePresentation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, presentation, optionTypeName, optionTypeId, optionTypePresentation);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OptionValue {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    presentation: ").append(toIndentedString(presentation)).append("\n");
    sb.append("    optionTypeName: ").append(toIndentedString(optionTypeName)).append("\n");
    sb.append("    optionTypeId: ").append(toIndentedString(optionTypeId)).append("\n");
    sb.append("    optionTypePresentation: ").append(toIndentedString(optionTypePresentation)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

