package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import net.alphahash.marketplace.model.Address;
import net.alphahash.marketplace.model.LineItem;
import net.alphahash.marketplace.model.Payment;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Order
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

public class Order   {
  @JsonProperty("id")
  private Long id = null;

  @JsonProperty("number")
  private String number = null;

  @JsonProperty("item_total")
  private String itemTotal = null;

  @JsonProperty("total")
  private String total = null;

  @JsonProperty("ship_total")
  private String shipTotal = null;

  @JsonProperty("state")
  private String state = null;

  @JsonProperty("adjustment_total")
  private String adjustmentTotal = null;

  @JsonProperty("user_id")
  private Long userId = null;

  @JsonProperty("created_at")
  private String createdAt = null;

  @JsonProperty("updated_at")
  private String updatedAt = null;

  @JsonProperty("completed_at")
  private String completedAt = null;

  @JsonProperty("payment_total")
  private String paymentTotal = null;

  @JsonProperty("shipment_state")
  private String shipmentState = null;

  @JsonProperty("payment_state")
  private String paymentState = null;

  @JsonProperty("email")
  private String email = null;

  @JsonProperty("special_instructions")
  private String specialInstructions = null;

  @JsonProperty("channel")
  private String channel = null;

  @JsonProperty("included_tax_total")
  private String includedTaxTotal = null;

  @JsonProperty("additional_tax_total")
  private String additionalTaxTotal = null;

  @JsonProperty("display_included_tax_total")
  private String displayIncludedTaxTotal = null;

  @JsonProperty("display_additional_tax_total")
  private String displayAdditionalTaxTotal = null;

  @JsonProperty("tax_total")
  private String taxTotal = null;

  @JsonProperty("currency")
  private String currency = null;

  @JsonProperty("considered_risky")
  private String consideredRisky = null;

  @JsonProperty("canceler_id")
  private String cancelerId = null;

  @JsonProperty("display_item_total")
  private String displayItemTotal = null;

  @JsonProperty("total_quantity")
  private String totalQuantity = null;

  @JsonProperty("display_total")
  private String displayTotal = null;

  @JsonProperty("display_ship_total")
  private String displayShipTotal = null;

  @JsonProperty("display_tax_total")
  private String displayTaxTotal = null;

  @JsonProperty("display_adjustment_total")
  private String displayAdjustmentTotal = null;

  @JsonProperty("token")
  private String token = null;

  @JsonProperty("checkout_steps")
  private List<String> checkoutSteps = null;

  @JsonProperty("bill_address")
  @Valid
  private Address billAddress = null;

  @JsonProperty("ship_address")
  @Valid
  private Address shipAddress = null;

  @JsonProperty("line_items")
  @Valid
  private List<LineItem> lineItems = null;

  @JsonProperty("payments")
  @Valid
  private List<Payment> payments = null;

  @JsonProperty("shipments")
  private String shipments = null;

  @JsonProperty("adjustments")
  private String adjustments = null;

  @JsonProperty("credit_cards")
  private String creditCards = null;

  @JsonProperty("permissions")
  private Boolean permissions = null;

  public Order id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Order number(String number) {
    this.number = number;
    return this;
  }

  /**
   * Get number
   * @return number
  **/
  @ApiModelProperty(value = "")


  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public Order itemTotal(String itemTotal) {
    this.itemTotal = itemTotal;
    return this;
  }

  /**
   * Get itemTotal
   * @return itemTotal
  **/
  @ApiModelProperty(value = "")


  public String getItemTotal() {
    return itemTotal;
  }

  public void setItemTotal(String itemTotal) {
    this.itemTotal = itemTotal;
  }

  public Order total(String total) {
    this.total = total;
    return this;
  }

  /**
   * Get total
   * @return total
  **/
  @ApiModelProperty(value = "")


  public String getTotal() {
    return total;
  }

  public void setTotal(String total) {
    this.total = total;
  }

  public Order shipTotal(String shipTotal) {
    this.shipTotal = shipTotal;
    return this;
  }

  /**
   * Get shipTotal
   * @return shipTotal
  **/
  @ApiModelProperty(value = "")


  public String getShipTotal() {
    return shipTotal;
  }

  public void setShipTotal(String shipTotal) {
    this.shipTotal = shipTotal;
  }

  public Order state(String state) {
    this.state = state;
    return this;
  }

  /**
   * Get state
   * @return state
  **/
  @ApiModelProperty(value = "")


  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public Order adjustmentTotal(String adjustmentTotal) {
    this.adjustmentTotal = adjustmentTotal;
    return this;
  }

  /**
   * Get adjustmentTotal
   * @return adjustmentTotal
  **/
  @ApiModelProperty(value = "")


  public String getAdjustmentTotal() {
    return adjustmentTotal;
  }

  public void setAdjustmentTotal(String adjustmentTotal) {
    this.adjustmentTotal = adjustmentTotal;
  }

  public Order userId(Long userId) {
    this.userId = userId;
    return this;
  }

  /**
   * Get userId
   * @return userId
  **/
  @ApiModelProperty(value = "")


  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Order createdAt(String createdAt) {
    this.createdAt = createdAt;
    return this;
  }

  /**
   * Get createdAt
   * @return createdAt
  **/
  @ApiModelProperty(value = "")


  public String getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  public Order updatedAt(String updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

  /**
   * Get updatedAt
   * @return updatedAt
  **/
  @ApiModelProperty(value = "")


  public String getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(String updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Order completedAt(String completedAt) {
    this.completedAt = completedAt;
    return this;
  }

  /**
   * Get completedAt
   * @return completedAt
  **/
  @ApiModelProperty(value = "")


  public String getCompletedAt() {
    return completedAt;
  }

  public void setCompletedAt(String completedAt) {
    this.completedAt = completedAt;
  }

  public Order paymentTotal(String paymentTotal) {
    this.paymentTotal = paymentTotal;
    return this;
  }

  /**
   * Get paymentTotal
   * @return paymentTotal
  **/
  @ApiModelProperty(value = "")


  public String getPaymentTotal() {
    return paymentTotal;
  }

  public void setPaymentTotal(String paymentTotal) {
    this.paymentTotal = paymentTotal;
  }

  public Order shipmentState(String shipmentState) {
    this.shipmentState = shipmentState;
    return this;
  }

  /**
   * Get shipmentState
   * @return shipmentState
  **/
  @ApiModelProperty(value = "")


  public String getShipmentState() {
    return shipmentState;
  }

  public void setShipmentState(String shipmentState) {
    this.shipmentState = shipmentState;
  }

  public Order paymentState(String paymentState) {
    this.paymentState = paymentState;
    return this;
  }

  /**
   * Get paymentState
   * @return paymentState
  **/
  @ApiModelProperty(value = "")


  public String getPaymentState() {
    return paymentState;
  }

  public void setPaymentState(String paymentState) {
    this.paymentState = paymentState;
  }

  public Order email(String email) {
    this.email = email;
    return this;
  }

  /**
   * Get email
   * @return email
  **/
  @ApiModelProperty(value = "")


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Order specialInstructions(String specialInstructions) {
    this.specialInstructions = specialInstructions;
    return this;
  }

  /**
   * Get specialInstructions
   * @return specialInstructions
  **/
  @ApiModelProperty(value = "")


  public String getSpecialInstructions() {
    return specialInstructions;
  }

  public void setSpecialInstructions(String specialInstructions) {
    this.specialInstructions = specialInstructions;
  }

  public Order channel(String channel) {
    this.channel = channel;
    return this;
  }

  /**
   * Get channel
   * @return channel
  **/
  @ApiModelProperty(value = "")


  public String getChannel() {
    return channel;
  }

  public void setChannel(String channel) {
    this.channel = channel;
  }

  public Order includedTaxTotal(String includedTaxTotal) {
    this.includedTaxTotal = includedTaxTotal;
    return this;
  }

  /**
   * Get includedTaxTotal
   * @return includedTaxTotal
  **/
  @ApiModelProperty(value = "")


  public String getIncludedTaxTotal() {
    return includedTaxTotal;
  }

  public void setIncludedTaxTotal(String includedTaxTotal) {
    this.includedTaxTotal = includedTaxTotal;
  }

  public Order additionalTaxTotal(String additionalTaxTotal) {
    this.additionalTaxTotal = additionalTaxTotal;
    return this;
  }

  /**
   * Get additionalTaxTotal
   * @return additionalTaxTotal
  **/
  @ApiModelProperty(value = "")


  public String getAdditionalTaxTotal() {
    return additionalTaxTotal;
  }

  public void setAdditionalTaxTotal(String additionalTaxTotal) {
    this.additionalTaxTotal = additionalTaxTotal;
  }

  public Order displayIncludedTaxTotal(String displayIncludedTaxTotal) {
    this.displayIncludedTaxTotal = displayIncludedTaxTotal;
    return this;
  }

  /**
   * Get displayIncludedTaxTotal
   * @return displayIncludedTaxTotal
  **/
  @ApiModelProperty(value = "")


  public String getDisplayIncludedTaxTotal() {
    return displayIncludedTaxTotal;
  }

  public void setDisplayIncludedTaxTotal(String displayIncludedTaxTotal) {
    this.displayIncludedTaxTotal = displayIncludedTaxTotal;
  }

  public Order displayAdditionalTaxTotal(String displayAdditionalTaxTotal) {
    this.displayAdditionalTaxTotal = displayAdditionalTaxTotal;
    return this;
  }

  /**
   * Get displayAdditionalTaxTotal
   * @return displayAdditionalTaxTotal
  **/
  @ApiModelProperty(value = "")


  public String getDisplayAdditionalTaxTotal() {
    return displayAdditionalTaxTotal;
  }

  public void setDisplayAdditionalTaxTotal(String displayAdditionalTaxTotal) {
    this.displayAdditionalTaxTotal = displayAdditionalTaxTotal;
  }

  public Order taxTotal(String taxTotal) {
    this.taxTotal = taxTotal;
    return this;
  }

  /**
   * Get taxTotal
   * @return taxTotal
  **/
  @ApiModelProperty(value = "")


  public String getTaxTotal() {
    return taxTotal;
  }

  public void setTaxTotal(String taxTotal) {
    this.taxTotal = taxTotal;
  }

  public Order currency(String currency) {
    this.currency = currency;
    return this;
  }

  /**
   * Get currency
   * @return currency
  **/
  @ApiModelProperty(value = "")


  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public Order consideredRisky(String consideredRisky) {
    this.consideredRisky = consideredRisky;
    return this;
  }

  /**
   * Get consideredRisky
   * @return consideredRisky
  **/
  @ApiModelProperty(value = "")


  public String getConsideredRisky() {
    return consideredRisky;
  }

  public void setConsideredRisky(String consideredRisky) {
    this.consideredRisky = consideredRisky;
  }

  public Order cancelerId(String cancelerId) {
    this.cancelerId = cancelerId;
    return this;
  }

  /**
   * Get cancelerId
   * @return cancelerId
  **/
  @ApiModelProperty(value = "")


  public String getCancelerId() {
    return cancelerId;
  }

  public void setCancelerId(String cancelerId) {
    this.cancelerId = cancelerId;
  }

  public Order displayItemTotal(String displayItemTotal) {
    this.displayItemTotal = displayItemTotal;
    return this;
  }

  /**
   * Get displayItemTotal
   * @return displayItemTotal
  **/
  @ApiModelProperty(value = "")


  public String getDisplayItemTotal() {
    return displayItemTotal;
  }

  public void setDisplayItemTotal(String displayItemTotal) {
    this.displayItemTotal = displayItemTotal;
  }

  public Order totalQuantity(String totalQuantity) {
    this.totalQuantity = totalQuantity;
    return this;
  }

  /**
   * Get totalQuantity
   * @return totalQuantity
  **/
  @ApiModelProperty(value = "")


  public String getTotalQuantity() {
    return totalQuantity;
  }

  public void setTotalQuantity(String totalQuantity) {
    this.totalQuantity = totalQuantity;
  }

  public Order displayTotal(String displayTotal) {
    this.displayTotal = displayTotal;
    return this;
  }

  /**
   * Get displayTotal
   * @return displayTotal
  **/
  @ApiModelProperty(value = "")


  public String getDisplayTotal() {
    return displayTotal;
  }

  public void setDisplayTotal(String displayTotal) {
    this.displayTotal = displayTotal;
  }

  public Order displayShipTotal(String displayShipTotal) {
    this.displayShipTotal = displayShipTotal;
    return this;
  }

  /**
   * Get displayShipTotal
   * @return displayShipTotal
  **/
  @ApiModelProperty(value = "")


  public String getDisplayShipTotal() {
    return displayShipTotal;
  }

  public void setDisplayShipTotal(String displayShipTotal) {
    this.displayShipTotal = displayShipTotal;
  }

  public Order displayTaxTotal(String displayTaxTotal) {
    this.displayTaxTotal = displayTaxTotal;
    return this;
  }

  /**
   * Get displayTaxTotal
   * @return displayTaxTotal
  **/
  @ApiModelProperty(value = "")


  public String getDisplayTaxTotal() {
    return displayTaxTotal;
  }

  public void setDisplayTaxTotal(String displayTaxTotal) {
    this.displayTaxTotal = displayTaxTotal;
  }

  public Order displayAdjustmentTotal(String displayAdjustmentTotal) {
    this.displayAdjustmentTotal = displayAdjustmentTotal;
    return this;
  }

  /**
   * Get displayAdjustmentTotal
   * @return displayAdjustmentTotal
  **/
  @ApiModelProperty(value = "")


  public String getDisplayAdjustmentTotal() {
    return displayAdjustmentTotal;
  }

  public void setDisplayAdjustmentTotal(String displayAdjustmentTotal) {
    this.displayAdjustmentTotal = displayAdjustmentTotal;
  }

  public Order token(String token) {
    this.token = token;
    return this;
  }

  /**
   * Get token
   * @return token
  **/
  @ApiModelProperty(value = "")


  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public Order checkoutSteps(List<String> checkoutSteps) {
    this.checkoutSteps = checkoutSteps;
    return this;
  }

  /**
   * Get checkoutSteps
   * @return checkoutSteps
  **/
  @ApiModelProperty(value = "")


  public List<String> getCheckoutSteps() {
    return checkoutSteps;
  }

  public void setCheckoutSteps(List<String> checkoutSteps) {
    this.checkoutSteps = checkoutSteps;
  }

  public Order billAddress(Address billAddress) {
    this.billAddress = billAddress;
    return this;
  }


  /**
   * Get billAddress
   * @return billAddress
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Address getBillAddress() {
    return billAddress;
  }

  public void setBillAddress(Address billAddress) {
    this.billAddress = billAddress;
  }

  public Order shipAddress(Address shipAddress) {
    this.shipAddress = shipAddress;
    return this;
  }

  /**
   * Get shipAddress
   * @return shipAddress
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Address getShipAddress() {
    return shipAddress;
  }

  public void setShipAddress(Address shipAddress) {
    this.shipAddress = shipAddress;
  }

  public Order lineItems(List<LineItem> lineItems) {
    this.lineItems = lineItems;
    return this;
  }

  public Order addLineItemsItem(LineItem lineItemsItem) {
    if (this.lineItems == null) {
      this.lineItems = new ArrayList<LineItem>();
    }
    this.lineItems.add(lineItemsItem);
    return this;
  }

  /**
   * Get lineItems
   * @return lineItems
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<LineItem> getLineItems() {
    return lineItems;
  }

  public void setLineItems(List<LineItem> lineItems) {
    this.lineItems = lineItems;
  }

  public Order payments(List<Payment> payments) {
    this.payments = payments;
    return this;
  }

  public Order addPaymentsItem(Payment paymentsItem) {
    if (this.payments == null) {
      this.payments = new ArrayList<Payment>();
    }
    this.payments.add(paymentsItem);
    return this;
  }

  /**
   * Get payments
   * @return payments
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Payment> getPayments() {
    return payments;
  }

  public void setPayments(List<Payment> payments) {
    this.payments = payments;
  }

  public Order shipments(String shipments) {
    this.shipments = shipments;
    return this;
  }

  /**
   * Get shipments
   * @return shipments
  **/
  @ApiModelProperty(value = "")


  public String getShipments() {
    return shipments;
  }

  public void setShipments(String shipments) {
    this.shipments = shipments;
  }

  public Order adjustments(String adjustments) {
    this.adjustments = adjustments;
    return this;
  }

  /**
   * Get adjustments
   * @return adjustments
  **/
  @ApiModelProperty(value = "")


  public String getAdjustments() {
    return adjustments;
  }

  public void setAdjustments(String adjustments) {
    this.adjustments = adjustments;
  }

  public Order creditCards(String creditCards) {
    this.creditCards = creditCards;
    return this;
  }

  /**
   * Get creditCards
   * @return creditCards
  **/
  @ApiModelProperty(value = "")


  public String getCreditCards() {
    return creditCards;
  }

  public void setCreditCards(String creditCards) {
    this.creditCards = creditCards;
  }

  public Order permissions(Boolean permissions) {
    this.permissions = permissions;
    return this;
  }

  /**
   * Get permissions
   * @return permissions
  **/
  @ApiModelProperty(value = "")


  public Boolean getPermissions() {
    return permissions;
  }

  public void setPermissions(Boolean permissions) {
    this.permissions = permissions;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Order order = (Order) o;
    return Objects.equals(this.id, order.id) &&
        Objects.equals(this.number, order.number) &&
        Objects.equals(this.itemTotal, order.itemTotal) &&
        Objects.equals(this.total, order.total) &&
        Objects.equals(this.shipTotal, order.shipTotal) &&
        Objects.equals(this.state, order.state) &&
        Objects.equals(this.adjustmentTotal, order.adjustmentTotal) &&
        Objects.equals(this.userId, order.userId) &&
        Objects.equals(this.createdAt, order.createdAt) &&
        Objects.equals(this.updatedAt, order.updatedAt) &&
        Objects.equals(this.completedAt, order.completedAt) &&
        Objects.equals(this.paymentTotal, order.paymentTotal) &&
        Objects.equals(this.shipmentState, order.shipmentState) &&
        Objects.equals(this.paymentState, order.paymentState) &&
        Objects.equals(this.email, order.email) &&
        Objects.equals(this.specialInstructions, order.specialInstructions) &&
        Objects.equals(this.channel, order.channel) &&
        Objects.equals(this.includedTaxTotal, order.includedTaxTotal) &&
        Objects.equals(this.additionalTaxTotal, order.additionalTaxTotal) &&
        Objects.equals(this.displayIncludedTaxTotal, order.displayIncludedTaxTotal) &&
        Objects.equals(this.displayAdditionalTaxTotal, order.displayAdditionalTaxTotal) &&
        Objects.equals(this.taxTotal, order.taxTotal) &&
        Objects.equals(this.currency, order.currency) &&
        Objects.equals(this.consideredRisky, order.consideredRisky) &&
        Objects.equals(this.cancelerId, order.cancelerId) &&
        Objects.equals(this.displayItemTotal, order.displayItemTotal) &&
        Objects.equals(this.totalQuantity, order.totalQuantity) &&
        Objects.equals(this.displayTotal, order.displayTotal) &&
        Objects.equals(this.displayShipTotal, order.displayShipTotal) &&
        Objects.equals(this.displayTaxTotal, order.displayTaxTotal) &&
        Objects.equals(this.displayAdjustmentTotal, order.displayAdjustmentTotal) &&
        Objects.equals(this.token, order.token) &&
        Objects.equals(this.checkoutSteps, order.checkoutSteps) &&
        Objects.equals(this.billAddress, order.billAddress) &&
        Objects.equals(this.shipAddress, order.shipAddress) &&
        Objects.equals(this.lineItems, order.lineItems) &&
        Objects.equals(this.payments, order.payments) &&
        Objects.equals(this.shipments, order.shipments) &&
        Objects.equals(this.adjustments, order.adjustments) &&
        Objects.equals(this.creditCards, order.creditCards) &&
        Objects.equals(this.permissions, order.permissions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, number, itemTotal, total, shipTotal, state, adjustmentTotal, userId, createdAt, updatedAt, completedAt, paymentTotal, shipmentState, paymentState, email, specialInstructions, channel, includedTaxTotal, additionalTaxTotal, displayIncludedTaxTotal, displayAdditionalTaxTotal, taxTotal, currency, consideredRisky, cancelerId, displayItemTotal, totalQuantity, displayTotal, displayShipTotal, displayTaxTotal, displayAdjustmentTotal, token, checkoutSteps, billAddress, shipAddress, lineItems, payments, shipments, adjustments, creditCards, permissions);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Order {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    number: ").append(toIndentedString(number)).append("\n");
    sb.append("    itemTotal: ").append(toIndentedString(itemTotal)).append("\n");
    sb.append("    total: ").append(toIndentedString(total)).append("\n");
    sb.append("    shipTotal: ").append(toIndentedString(shipTotal)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    adjustmentTotal: ").append(toIndentedString(adjustmentTotal)).append("\n");
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("    completedAt: ").append(toIndentedString(completedAt)).append("\n");
    sb.append("    paymentTotal: ").append(toIndentedString(paymentTotal)).append("\n");
    sb.append("    shipmentState: ").append(toIndentedString(shipmentState)).append("\n");
    sb.append("    paymentState: ").append(toIndentedString(paymentState)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    specialInstructions: ").append(toIndentedString(specialInstructions)).append("\n");
    sb.append("    channel: ").append(toIndentedString(channel)).append("\n");
    sb.append("    includedTaxTotal: ").append(toIndentedString(includedTaxTotal)).append("\n");
    sb.append("    additionalTaxTotal: ").append(toIndentedString(additionalTaxTotal)).append("\n");
    sb.append("    displayIncludedTaxTotal: ").append(toIndentedString(displayIncludedTaxTotal)).append("\n");
    sb.append("    displayAdditionalTaxTotal: ").append(toIndentedString(displayAdditionalTaxTotal)).append("\n");
    sb.append("    taxTotal: ").append(toIndentedString(taxTotal)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("    consideredRisky: ").append(toIndentedString(consideredRisky)).append("\n");
    sb.append("    cancelerId: ").append(toIndentedString(cancelerId)).append("\n");
    sb.append("    displayItemTotal: ").append(toIndentedString(displayItemTotal)).append("\n");
    sb.append("    totalQuantity: ").append(toIndentedString(totalQuantity)).append("\n");
    sb.append("    displayTotal: ").append(toIndentedString(displayTotal)).append("\n");
    sb.append("    displayShipTotal: ").append(toIndentedString(displayShipTotal)).append("\n");
    sb.append("    displayTaxTotal: ").append(toIndentedString(displayTaxTotal)).append("\n");
    sb.append("    displayAdjustmentTotal: ").append(toIndentedString(displayAdjustmentTotal)).append("\n");
    sb.append("    token: ").append(toIndentedString(token)).append("\n");
    sb.append("    checkoutSteps: ").append(toIndentedString(checkoutSteps)).append("\n");
    sb.append("    billAddress: ").append(toIndentedString(billAddress)).append("\n");
    sb.append("    shipAddress: ").append(toIndentedString(shipAddress)).append("\n");
    sb.append("    lineItems: ").append(toIndentedString(lineItems)).append("\n");
    sb.append("    payments: ").append(toIndentedString(payments)).append("\n");
    sb.append("    shipments: ").append(toIndentedString(shipments)).append("\n");
    sb.append("    adjustments: ").append(toIndentedString(adjustments)).append("\n");
    sb.append("    creditCards: ").append(toIndentedString(creditCards)).append("\n");
    sb.append("    permissions: ").append(toIndentedString(permissions)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

