package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import net.alphahash.marketplace.model.Taxons;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Taxons
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

public class Taxons   {
  @JsonProperty("id")
  private Long id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("pretty_name")
  private String prettyName = null;

  @JsonProperty("permalink")
  private String permalink = null;

  @JsonProperty("parent_id")
  private Long parentId = null;

  @JsonProperty("taxonomy_id")
  private Long taxonomyId = null;

  @JsonProperty("meta_title")
  private String metaTitle = null;

  @JsonProperty("meta_description")
  private String metaDescription = null;

  @JsonProperty("taxons")
  @Valid
  private List<Taxons> taxons = null;

  @JsonProperty("icon")
  private String icon = null;

  public Taxons id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Taxons name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Taxons prettyName(String prettyName) {
    this.prettyName = prettyName;
    return this;
  }

  /**
   * Get prettyName
   * @return prettyName
  **/
  @ApiModelProperty(value = "")


  public String getPrettyName() {
    return prettyName;
  }

  public void setPrettyName(String prettyName) {
    this.prettyName = prettyName;
  }

  public Taxons permalink(String permalink) {
    this.permalink = permalink;
    return this;
  }

  /**
   * Get permalink
   * @return permalink
  **/
  @ApiModelProperty(value = "")


  public String getPermalink() {
    return permalink;
  }

  public void setPermalink(String permalink) {
    this.permalink = permalink;
  }

  public Taxons parentId(Long parentId) {
    this.parentId = parentId;
    return this;
  }

  /**
   * Get parentId
   * @return parentId
  **/
  @ApiModelProperty(value = "")


  public Long getParentId() {
    return parentId;
  }

  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }

  public Taxons taxonomyId(Long taxonomyId) {
    this.taxonomyId = taxonomyId;
    return this;
  }

  /**
   * Get taxonomyId
   * @return taxonomyId
  **/
  @ApiModelProperty(value = "")


  public Long getTaxonomyId() {
    return taxonomyId;
  }

  public void setTaxonomyId(Long taxonomyId) {
    this.taxonomyId = taxonomyId;
  }

  public Taxons metaTitle(String metaTitle) {
    this.metaTitle = metaTitle;
    return this;
  }

  /**
   * Get metaTitle
   * @return metaTitle
  **/
  @ApiModelProperty(value = "")


  public String getMetaTitle() {
    return metaTitle;
  }

  public void setMetaTitle(String metaTitle) {
    this.metaTitle = metaTitle;
  }

  public Taxons metaDescription(String metaDescription) {
    this.metaDescription = metaDescription;
    return this;
  }

  /**
   * Get metaDescription
   * @return metaDescription
  **/
  @ApiModelProperty(value = "")


  public String getMetaDescription() {
    return metaDescription;
  }

  public void setMetaDescription(String metaDescription) {
    this.metaDescription = metaDescription;
  }

  public Taxons taxons(List<Taxons> taxons) {
    this.taxons = taxons;
    return this;
  }

  public Taxons addTaxonsItem(Taxons taxonsItem) {
    if (this.taxons == null) {
      this.taxons = new ArrayList<Taxons>();
    }
    this.taxons.add(taxonsItem);
    return this;
  }

  /**
   * Get taxons
   * @return taxons
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Taxons> getTaxons() {
    return taxons;
  }

  public void setTaxons(List<Taxons> taxons) {
    this.taxons = taxons;
  }

  public Taxons icon(String icon) {
    this.icon = icon;
    return this;
  }

  /**
   * Get icon
   * @return icon
  **/
  @ApiModelProperty(value = "")


  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Taxons taxons = (Taxons) o;
    return Objects.equals(this.id, taxons.id) &&
        Objects.equals(this.name, taxons.name) &&
        Objects.equals(this.prettyName, taxons.prettyName) &&
        Objects.equals(this.permalink, taxons.permalink) &&
        Objects.equals(this.parentId, taxons.parentId) &&
        Objects.equals(this.taxonomyId, taxons.taxonomyId) &&
        Objects.equals(this.metaTitle, taxons.metaTitle) &&
        Objects.equals(this.metaDescription, taxons.metaDescription) &&
        Objects.equals(this.taxons, taxons.taxons) &&
        Objects.equals(this.icon, taxons.icon);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, prettyName, permalink, parentId, taxonomyId, metaTitle, metaDescription, taxons, icon);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Taxons {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    prettyName: ").append(toIndentedString(prettyName)).append("\n");
    sb.append("    permalink: ").append(toIndentedString(permalink)).append("\n");
    sb.append("    parentId: ").append(toIndentedString(parentId)).append("\n");
    sb.append("    taxonomyId: ").append(toIndentedString(taxonomyId)).append("\n");
    sb.append("    metaTitle: ").append(toIndentedString(metaTitle)).append("\n");
    sb.append("    metaDescription: ").append(toIndentedString(metaDescription)).append("\n");
    sb.append("    taxons: ").append(toIndentedString(taxons)).append("\n");
    sb.append("    icon: ").append(toIndentedString(icon)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

