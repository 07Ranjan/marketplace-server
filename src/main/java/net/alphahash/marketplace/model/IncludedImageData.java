package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * IncludedImageData
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-09-01T13:32:21.812+01:00")

public class IncludedImageData   {
  @JsonProperty("id")
  private BigDecimal id = null;

  @JsonProperty("position")
  private BigDecimal position = null;

  @JsonProperty("attachment_content_type")
  private String attachmentContentType = null;

  @JsonProperty("attachment_file_name")
  private String attachmentFileName = null;

  @JsonProperty("type")
  private String type = null;

  @JsonProperty("attachment_updated_at")
  private String attachmentUpdatedAt = null;

  @JsonProperty("attachment_width")
  private String attachmentWidth = null;

  @JsonProperty("attachment_height")
  private String attachmentHeight = null;

  @JsonProperty("alt")
  private String alt = null;

  @JsonProperty("viewable_type")
  private String viewableType = null;

  @JsonProperty("viewable_id")
  private BigDecimal viewableId = null;

  @JsonProperty("mini_url")
  private String miniUrl = null;

  @JsonProperty("small_url")
  private String smallUrl = null;

  @JsonProperty("product_url")
  private String productUrl = null;

  @JsonProperty("large_url")
  private String largeUrl = null;

  public IncludedImageData id(BigDecimal id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getId() {
    return id;
  }

  public void setId(BigDecimal id) {
    this.id = id;
  }

  public IncludedImageData position(BigDecimal position) {
    this.position = position;
    return this;
  }

  /**
   * Get position
   * @return position
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getPosition() {
    return position;
  }

  public void setPosition(BigDecimal position) {
    this.position = position;
  }

  public IncludedImageData attachmentContentType(String attachmentContentType) {
    this.attachmentContentType = attachmentContentType;
    return this;
  }

  /**
   * Get attachmentContentType
   * @return attachmentContentType
  **/
  @ApiModelProperty(value = "")


  public String getAttachmentContentType() {
    return attachmentContentType;
  }

  public void setAttachmentContentType(String attachmentContentType) {
    this.attachmentContentType = attachmentContentType;
  }

  public IncludedImageData attachmentFileName(String attachmentFileName) {
    this.attachmentFileName = attachmentFileName;
    return this;
  }

  /**
   * Get attachmentFileName
   * @return attachmentFileName
  **/
  @ApiModelProperty(value = "")


  public String getAttachmentFileName() {
    return attachmentFileName;
  }

  public void setAttachmentFileName(String attachmentFileName) {
    this.attachmentFileName = attachmentFileName;
  }

  public IncludedImageData type(String type) {
    this.type = type;
    return this;
  }

  /**
   * Get type
   * @return type
  **/
  @ApiModelProperty(value = "")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public IncludedImageData attachmentUpdatedAt(String attachmentUpdatedAt) {
    this.attachmentUpdatedAt = attachmentUpdatedAt;
    return this;
  }

  /**
   * Get attachmentUpdatedAt
   * @return attachmentUpdatedAt
  **/
  @ApiModelProperty(value = "")


  public String getAttachmentUpdatedAt() {
    return attachmentUpdatedAt;
  }

  public void setAttachmentUpdatedAt(String attachmentUpdatedAt) {
    this.attachmentUpdatedAt = attachmentUpdatedAt;
  }

  public IncludedImageData attachmentWidth(String attachmentWidth) {
    this.attachmentWidth = attachmentWidth;
    return this;
  }

  /**
   * Get attachmentWidth
   * @return attachmentWidth
  **/
  @ApiModelProperty(value = "")


  public String getAttachmentWidth() {
    return attachmentWidth;
  }

  public void setAttachmentWidth(String attachmentWidth) {
    this.attachmentWidth = attachmentWidth;
  }

  public IncludedImageData attachmentHeight(String attachmentHeight) {
    this.attachmentHeight = attachmentHeight;
    return this;
  }

  /**
   * Get attachmentHeight
   * @return attachmentHeight
  **/
  @ApiModelProperty(value = "")


  public String getAttachmentHeight() {
    return attachmentHeight;
  }

  public void setAttachmentHeight(String attachmentHeight) {
    this.attachmentHeight = attachmentHeight;
  }

  public IncludedImageData alt(String alt) {
    this.alt = alt;
    return this;
  }

  /**
   * Get alt
   * @return alt
  **/
  @ApiModelProperty(value = "")


  public String getAlt() {
    return alt;
  }

  public void setAlt(String alt) {
    this.alt = alt;
  }

  public IncludedImageData viewableType(String viewableType) {
    this.viewableType = viewableType;
    return this;
  }

  /**
   * Get viewableType
   * @return viewableType
  **/
  @ApiModelProperty(value = "")


  public String getViewableType() {
    return viewableType;
  }

  public void setViewableType(String viewableType) {
    this.viewableType = viewableType;
  }

  public IncludedImageData viewableId(BigDecimal viewableId) {
    this.viewableId = viewableId;
    return this;
  }

  /**
   * Get viewableId
   * @return viewableId
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getViewableId() {
    return viewableId;
  }

  public void setViewableId(BigDecimal viewableId) {
    this.viewableId = viewableId;
  }

  public IncludedImageData miniUrl(String miniUrl) {
    this.miniUrl = miniUrl;
    return this;
  }

  /**
   * Get miniUrl
   * @return miniUrl
  **/
  @ApiModelProperty(value = "")


  public String getMiniUrl() {
    return miniUrl;
  }

  public void setMiniUrl(String miniUrl) {
    this.miniUrl = miniUrl;
  }

  public IncludedImageData smallUrl(String smallUrl) {
    this.smallUrl = smallUrl;
    return this;
  }

  /**
   * Get smallUrl
   * @return smallUrl
  **/
  @ApiModelProperty(value = "")


  public String getSmallUrl() {
    return smallUrl;
  }

  public void setSmallUrl(String smallUrl) {
    this.smallUrl = smallUrl;
  }

  public IncludedImageData productUrl(String productUrl) {
    this.productUrl = productUrl;
    return this;
  }

  /**
   * Get productUrl
   * @return productUrl
  **/
  @ApiModelProperty(value = "")


  public String getProductUrl() {
    return productUrl;
  }

  public void setProductUrl(String productUrl) {
    this.productUrl = productUrl;
  }

  public IncludedImageData largeUrl(String largeUrl) {
    this.largeUrl = largeUrl;
    return this;
  }

  /**
   * Get largeUrl
   * @return largeUrl
  **/
  @ApiModelProperty(value = "")


  public String getLargeUrl() {
    return largeUrl;
  }

  public void setLargeUrl(String largeUrl) {
    this.largeUrl = largeUrl;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    IncludedImageData includedImageData = (IncludedImageData) o;
    return Objects.equals(this.id, includedImageData.id) &&
        Objects.equals(this.position, includedImageData.position) &&
        Objects.equals(this.attachmentContentType, includedImageData.attachmentContentType) &&
        Objects.equals(this.attachmentFileName, includedImageData.attachmentFileName) &&
        Objects.equals(this.type, includedImageData.type) &&
        Objects.equals(this.attachmentUpdatedAt, includedImageData.attachmentUpdatedAt) &&
        Objects.equals(this.attachmentWidth, includedImageData.attachmentWidth) &&
        Objects.equals(this.attachmentHeight, includedImageData.attachmentHeight) &&
        Objects.equals(this.alt, includedImageData.alt) &&
        Objects.equals(this.viewableType, includedImageData.viewableType) &&
        Objects.equals(this.viewableId, includedImageData.viewableId) &&
        Objects.equals(this.miniUrl, includedImageData.miniUrl) &&
        Objects.equals(this.smallUrl, includedImageData.smallUrl) &&
        Objects.equals(this.productUrl, includedImageData.productUrl) &&
        Objects.equals(this.largeUrl, includedImageData.largeUrl);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, position, attachmentContentType, attachmentFileName, type, attachmentUpdatedAt, attachmentWidth, attachmentHeight, alt, viewableType, viewableId, miniUrl, smallUrl, productUrl, largeUrl);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class IncludedImageData {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    position: ").append(toIndentedString(position)).append("\n");
    sb.append("    attachmentContentType: ").append(toIndentedString(attachmentContentType)).append("\n");
    sb.append("    attachmentFileName: ").append(toIndentedString(attachmentFileName)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    attachmentUpdatedAt: ").append(toIndentedString(attachmentUpdatedAt)).append("\n");
    sb.append("    attachmentWidth: ").append(toIndentedString(attachmentWidth)).append("\n");
    sb.append("    attachmentHeight: ").append(toIndentedString(attachmentHeight)).append("\n");
    sb.append("    alt: ").append(toIndentedString(alt)).append("\n");
    sb.append("    viewableType: ").append(toIndentedString(viewableType)).append("\n");
    sb.append("    viewableId: ").append(toIndentedString(viewableId)).append("\n");
    sb.append("    miniUrl: ").append(toIndentedString(miniUrl)).append("\n");
    sb.append("    smallUrl: ").append(toIndentedString(smallUrl)).append("\n");
    sb.append("    productUrl: ").append(toIndentedString(productUrl)).append("\n");
    sb.append("    largeUrl: ").append(toIndentedString(largeUrl)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

