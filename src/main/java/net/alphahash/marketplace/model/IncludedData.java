package net.alphahash.marketplace.model;

import java.util.List;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.alphahash.marketplace.model.DataArray;
import net.alphahash.marketplace.model.IncludedImage;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * IncludedData
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-09-01T13:32:21.812+01:00")

public class IncludedData   {
  @JsonProperty("option_values")
  private List<Data> optionValues = null;

  @JsonProperty("images")
  private List<IncludedImage> images = null;

  public IncludedData optionValues(List<Data> optionValues) {
    this.optionValues = optionValues;
    return this;
  }

  /**
   * Get optionValues
   * @return optionValues
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Data> getOptionValues() {
    return optionValues;
  }

  public void setOptionValues(List<Data> optionValues) {
    this.optionValues = optionValues;
  }

  public IncludedData images(List<IncludedImage> images) {
    this.images = images;
    return this;
  }

  /**
   * Get images
   * @return images
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<IncludedImage> getImages() {
    return images;
  }

  public void setImages(List<IncludedImage> images) {
    this.images = images;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    IncludedData includedData = (IncludedData) o;
    return Objects.equals(this.optionValues, includedData.optionValues) &&
        Objects.equals(this.images, includedData.images);
  }

  @Override
  public int hashCode() {
    return Objects.hash(optionValues, images);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class IncludedData {\n");
    
    sb.append("    optionValues: ").append(toIndentedString(optionValues)).append("\n");
    sb.append("    images: ").append(toIndentedString(images)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

