package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Payment
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

public class Payment   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("source_type")
  private String sourceType = null;

  @JsonProperty("source_id")
  private String sourceId = null;

  @JsonProperty("amount")
  private String amount = null;

  @JsonProperty("display_amount")
  private String displayAmount = null;

  @JsonProperty("payment_method_id")
  private String paymentMethodId = null;

  @JsonProperty("response_code")
  private String responseCode = null;

  @JsonProperty("state")
  private String state = null;

  @JsonProperty("avs_response")
  private String avsResponse = null;

  @JsonProperty("created_at")
  private String createdAt = null;

  @JsonProperty("updated_at")
  private String updatedAt = null;

  public Payment id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Payment sourceType(String sourceType) {
    this.sourceType = sourceType;
    return this;
  }

  /**
   * Get sourceType
   * @return sourceType
  **/
  @ApiModelProperty(value = "")


  public String getSourceType() {
    return sourceType;
  }

  public void setSourceType(String sourceType) {
    this.sourceType = sourceType;
  }

  public Payment sourceId(String sourceId) {
    this.sourceId = sourceId;
    return this;
  }

  /**
   * Get sourceId
   * @return sourceId
  **/
  @ApiModelProperty(value = "")


  public String getSourceId() {
    return sourceId;
  }

  public void setSourceId(String sourceId) {
    this.sourceId = sourceId;
  }

  public Payment amount(String amount) {
    this.amount = amount;
    return this;
  }

  /**
   * Get amount
   * @return amount
  **/
  @ApiModelProperty(value = "")


  public String getAmount() {
    return amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }

  public Payment displayAmount(String displayAmount) {
    this.displayAmount = displayAmount;
    return this;
  }

  /**
   * Get displayAmount
   * @return displayAmount
  **/
  @ApiModelProperty(value = "")


  public String getDisplayAmount() {
    return displayAmount;
  }

  public void setDisplayAmount(String displayAmount) {
    this.displayAmount = displayAmount;
  }

  public Payment paymentMethodId(String paymentMethodId) {
    this.paymentMethodId = paymentMethodId;
    return this;
  }

  /**
   * Get paymentMethodId
   * @return paymentMethodId
  **/
  @ApiModelProperty(value = "")


  public String getPaymentMethodId() {
    return paymentMethodId;
  }

  public void setPaymentMethodId(String paymentMethodId) {
    this.paymentMethodId = paymentMethodId;
  }

  public Payment responseCode(String responseCode) {
    this.responseCode = responseCode;
    return this;
  }

  /**
   * Get responseCode
   * @return responseCode
  **/
  @ApiModelProperty(value = "")


  public String getResponseCode() {
    return responseCode;
  }

  public void setResponseCode(String responseCode) {
    this.responseCode = responseCode;
  }

  public Payment state(String state) {
    this.state = state;
    return this;
  }

  /**
   * Get state
   * @return state
  **/
  @ApiModelProperty(value = "")


  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public Payment avsResponse(String avsResponse) {
    this.avsResponse = avsResponse;
    return this;
  }

  /**
   * Get avsResponse
   * @return avsResponse
  **/
  @ApiModelProperty(value = "")


  public String getAvsResponse() {
    return avsResponse;
  }

  public void setAvsResponse(String avsResponse) {
    this.avsResponse = avsResponse;
  }

  public Payment createdAt(String createdAt) {
    this.createdAt = createdAt;
    return this;
  }

  /**
   * Get createdAt
   * @return createdAt
  **/
  @ApiModelProperty(value = "")


  public String getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  public Payment updatedAt(String updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

  /**
   * Get updatedAt
   * @return updatedAt
  **/
  @ApiModelProperty(value = "")


  public String getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(String updatedAt) {
    this.updatedAt = updatedAt;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Payment payment = (Payment) o;
    return Objects.equals(this.id, payment.id) &&
        Objects.equals(this.sourceType, payment.sourceType) &&
        Objects.equals(this.sourceId, payment.sourceId) &&
        Objects.equals(this.amount, payment.amount) &&
        Objects.equals(this.displayAmount, payment.displayAmount) &&
        Objects.equals(this.paymentMethodId, payment.paymentMethodId) &&
        Objects.equals(this.responseCode, payment.responseCode) &&
        Objects.equals(this.state, payment.state) &&
        Objects.equals(this.avsResponse, payment.avsResponse) &&
        Objects.equals(this.createdAt, payment.createdAt) &&
        Objects.equals(this.updatedAt, payment.updatedAt);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, sourceType, sourceId, amount, displayAmount, paymentMethodId, responseCode, state, avsResponse, createdAt, updatedAt);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Payment {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    sourceType: ").append(toIndentedString(sourceType)).append("\n");
    sb.append("    sourceId: ").append(toIndentedString(sourceId)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    displayAmount: ").append(toIndentedString(displayAmount)).append("\n");
    sb.append("    paymentMethodId: ").append(toIndentedString(paymentMethodId)).append("\n");
    sb.append("    responseCode: ").append(toIndentedString(responseCode)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    avsResponse: ").append(toIndentedString(avsResponse)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

