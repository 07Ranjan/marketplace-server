package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import net.alphahash.marketplace.model.Taxons;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Classification
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

public class Classification   {
  @JsonProperty("taxon_id")
  private BigDecimal taxonId = null;

  @JsonProperty("position")
  private BigDecimal position = null;

  @JsonProperty("taxon")
  private Taxons taxon = null;

  public Classification taxonId(BigDecimal taxonId) {
    this.taxonId = taxonId;
    return this;
  }

  /**
   * Get taxonId
   * @return taxonId
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getTaxonId() {
    return taxonId;
  }

  public void setTaxonId(BigDecimal taxonId) {
    this.taxonId = taxonId;
  }

  public Classification position(BigDecimal position) {
    this.position = position;
    return this;
  }

  /**
   * Get position
   * @return position
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getPosition() {
    return position;
  }

  public void setPosition(BigDecimal position) {
    this.position = position;
  }

  public Classification taxon(Taxons taxon) {
    this.taxon = taxon;
    return this;
  }

  /**
   * Get taxon
   * @return taxon
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Taxons getTaxon() {
    return taxon;
  }

  public void setTaxon(Taxons taxon) {
    this.taxon = taxon;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Classification classification = (Classification) o;
    return Objects.equals(this.taxonId, classification.taxonId) &&
        Objects.equals(this.position, classification.position) &&
        Objects.equals(this.taxon, classification.taxon);
  }

  @Override
  public int hashCode() {
    return Objects.hash(taxonId, position, taxon);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Classification {\n");
    
    sb.append("    taxonId: ").append(toIndentedString(taxonId)).append("\n");
    sb.append("    position: ").append(toIndentedString(position)).append("\n");
    sb.append("    taxon: ").append(toIndentedString(taxon)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

