package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import net.alphahash.marketplace.model.PaymentMode;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * PaymentModeArray
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-09-03T12:03:31.435+01:00")

public class PaymentModeArray   {
  @JsonProperty("payment_methods")
  @Valid
  private List<PaymentMode> paymentMethods = null;

  @JsonProperty("attributes")
  @Valid
  private List<String> attributes = null;

  public PaymentModeArray paymentMethods(List<PaymentMode> paymentMethods) {
    this.paymentMethods = paymentMethods;
    return this;
  }

  public PaymentModeArray addPaymentMethodsItem(PaymentMode paymentMethodsItem) {
    if (this.paymentMethods == null) {
      this.paymentMethods = new ArrayList<PaymentMode>();
    }
    this.paymentMethods.add(paymentMethodsItem);
    return this;
  }

  /**
   * Get paymentMethods
   * @return paymentMethods
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<PaymentMode> getPaymentMethods() {
    return paymentMethods;
  }

  public void setPaymentMethods(List<PaymentMode> paymentMethods) {
    this.paymentMethods = paymentMethods;
  }

  public PaymentModeArray attributes(List<String> attributes) {
    this.attributes = attributes;
    return this;
  }

  public PaymentModeArray addAttributesItem(String attributesItem) {
    if (this.attributes == null) {
      this.attributes = new ArrayList<String>();
    }
    this.attributes.add(attributesItem);
    return this;
  }

  /**
   * Get attributes
   * @return attributes
  **/
  @ApiModelProperty(value = "")


  public List<String> getAttributes() {
    return attributes;
  }

  public void setAttributes(List<String> attributes) {
    this.attributes = attributes;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentModeArray paymentModeArray = (PaymentModeArray) o;
    return Objects.equals(this.paymentMethods, paymentModeArray.paymentMethods) &&
        Objects.equals(this.attributes, paymentModeArray.attributes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(paymentMethods, attributes);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentModeArray {\n");
    
    sb.append("    paymentMethods: ").append(toIndentedString(paymentMethods)).append("\n");
    sb.append("    attributes: ").append(toIndentedString(attributes)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

