package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.alphahash.marketplace.model.AttributesData;
import net.alphahash.marketplace.model.IncludedData;
import net.alphahash.marketplace.model.RelationshipsData;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * MasterData
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-09-01T13:32:21.812+01:00")

public class MasterData   {
  @JsonProperty("attributes")
  private AttributesData attributes = null;

  @JsonProperty("relationships")
  private RelationshipsData relationships = null;

  @JsonProperty("included")
  private IncludedData included = null;

  public MasterData attributes(AttributesData attributes) {
    this.attributes = attributes;
    return this;
  }

  /**
   * Get attributes
   * @return attributes
  **/
  @ApiModelProperty(value = "")

  @Valid

  public AttributesData getAttributes() {
    return attributes;
  }

  public void setAttributes(AttributesData attributes) {
    this.attributes = attributes;
  }

  public MasterData relationships(RelationshipsData relationships) {
    this.relationships = relationships;
    return this;
  }

  /**
   * Get relationships
   * @return relationships
  **/
  @ApiModelProperty(value = "")

  @Valid

  public RelationshipsData getRelationships() {
    return relationships;
  }

  public void setRelationships(RelationshipsData relationships) {
    this.relationships = relationships;
  }

  public MasterData included(IncludedData included) {
    this.included = included;
    return this;
  }

  /**
   * Get included
   * @return included
  **/
  @ApiModelProperty(value = "")

  @Valid

  public IncludedData getIncluded() {
    return included;
  }

  public void setIncluded(IncludedData included) {
    this.included = included;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MasterData masterData = (MasterData) o;
    return Objects.equals(this.attributes, masterData.attributes) &&
        Objects.equals(this.relationships, masterData.relationships) &&
        Objects.equals(this.included, masterData.included);
  }

  @Override
  public int hashCode() {
    return Objects.hash(attributes, relationships, included);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MasterData {\n");
    
    sb.append("    attributes: ").append(toIndentedString(attributes)).append("\n");
    sb.append("    relationships: ").append(toIndentedString(relationships)).append("\n");
    sb.append("    included: ").append(toIndentedString(included)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

