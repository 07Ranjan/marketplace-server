package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.alphahash.marketplace.model.LineItemBody;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * LineItemDetail
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

public class LineItemDetail   {
  @JsonProperty("line_item")
  private LineItemBody lineItem = null;

  public LineItemDetail lineItem(LineItemBody lineItem) {
    this.lineItem = lineItem;
    return this;
  }

  /**
   * Get lineItem
   * @return lineItem
  **/
  @ApiModelProperty(value = "")

  @Valid

  public LineItemBody getLineItem() {
    return lineItem;
  }

  public void setLineItem(LineItemBody lineItem) {
    this.lineItem = lineItem;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LineItemDetail lineItemDetail = (LineItemDetail) o;
    return Objects.equals(this.lineItem, lineItemDetail.lineItem);
  }

  @Override
  public int hashCode() {
    return Objects.hash(lineItem);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LineItemDetail {\n");
    
    sb.append("    lineItem: ").append(toIndentedString(lineItem)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

