package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ProductAttribures
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

public class ProductAttribures   {
  @JsonProperty("slug")
  private String slug = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("product_url")
  private String productUrl = null;

  @JsonProperty("price")
  private String price = null;

  @JsonProperty("cost_price")
  private String costPrice = null;

  @JsonProperty("avg_rating")
  private String avgRating = null;

  @JsonProperty("reviews_count")
  private String reviewsCount = null;

  @JsonProperty("currency")
  private String currency = null;

  @JsonProperty("currency_symbol")
  private String currencySymbol = null;

  public ProductAttribures slug(String slug) {
    this.slug = slug;
    return this;
  }

  /**
   * Get slug
   * @return slug
  **/
  @ApiModelProperty(value = "")


  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

  public ProductAttribures name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ProductAttribures productUrl(String productUrl) {
    this.productUrl = productUrl;
    return this;
  }

  /**
   * Get productUrl
   * @return productUrl
  **/
  @ApiModelProperty(value = "")


  public String getProductUrl() {
    return productUrl;
  }

  public void setProductUrl(String productUrl) {
    this.productUrl = productUrl;
  }

  public ProductAttribures price(String price) {
    this.price = price;
    return this;
  }

  /**
   * Get price
   * @return price
  **/
  @ApiModelProperty(value = "")


  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public ProductAttribures costPrice(String costPrice) {
    this.costPrice = costPrice;
    return this;
  }

  /**
   * Get costPrice
   * @return costPrice
  **/
  @ApiModelProperty(value = "")


  public String getCostPrice() {
    return costPrice;
  }

  public void setCostPrice(String costPrice) {
    this.costPrice = costPrice;
  }

  public ProductAttribures currency(String currency) {
    this.currency = currency;
    return this;
  }

  /**
   * Get currency
   * @return currency
  **/
  @ApiModelProperty(value = "")


  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public String getAvgRating() {
    return avgRating;
  }

  public void setAvgRating(String avgRating) {
    this.avgRating = avgRating;
  }

  public String getReviewsCount() {
    return reviewsCount;
  }

  public void setReviewsCount(String reviewsCount) {
    this.reviewsCount = reviewsCount;
  }

  public String getCurrencySymbol() {
    return currencySymbol;
  }

  public void setCurrencySymbol(String currencySymbol) {
    this.currencySymbol = currencySymbol;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProductAttribures productAttribures = (ProductAttribures) o;
    return Objects.equals(this.slug, productAttribures.slug) &&
        Objects.equals(this.name, productAttribures.name) &&
        Objects.equals(this.productUrl, productAttribures.productUrl) &&
        Objects.equals(this.price, productAttribures.price) &&
        Objects.equals(this.costPrice, productAttribures.costPrice) &&
        Objects.equals(this.avgRating, productAttribures.avgRating) &&
        Objects.equals(this.reviewsCount, productAttribures.reviewsCount) &&
        Objects.equals(this.currencySymbol, productAttribures.currencySymbol) &&
        Objects.equals(this.currency, productAttribures.currency);
  }

  @Override
  public int hashCode() {
    return Objects.hash(slug, name, productUrl, price, costPrice, avgRating, reviewsCount, currency, currencySymbol);
  }



  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProductAttribures {\n");

    sb.append("    slug: ").append(toIndentedString(slug)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    productUrl: ").append(toIndentedString(productUrl)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("    costPrice: ").append(toIndentedString(costPrice)).append("\n");
    sb.append("    avgRating: ").append(toIndentedString(avgRating)).append("\n");
    sb.append("    reviewsCount: ").append(toIndentedString(reviewsCount)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("    currencySymbol: ").append(toIndentedString(currencySymbol)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

