package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * AttributesData
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-09-01T13:32:21.812+01:00")

public class AttributesData   {
  @JsonProperty("id")
  private BigDecimal id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("sku")
  private String sku = null;

  @JsonProperty("price")
  private String price = null;

  @JsonProperty("weight")
  private String weight = null;

  @JsonProperty("height")
  private String height = null;

  @JsonProperty("width")
  private String width = null;

  @JsonProperty("depth")
  private String depth = null;

  @JsonProperty("is_master")
  private Boolean isMaster = null;

  @JsonProperty("slug")
  private String slug = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("track_inventory")
  private Boolean trackInventory = null;

  @JsonProperty("display_price")
  private String displayPrice = null;

  @JsonProperty("options_text")
  private String optionsText = null;

  @JsonProperty("in_stock")
  private Boolean inStock = null;

  @JsonProperty("is_backorderable")
  private Boolean isBackorderable = null;

  @JsonProperty("is_orderable")
  private Boolean isOrderable = null;

  @JsonProperty("total_on_hand")
  private BigDecimal totalOnHand = null;

  @JsonProperty("is_destroyed")
  private Boolean isDestroyed = null;

  @JsonProperty("cost_price")
  private String costPrice = null;

  public AttributesData id(BigDecimal id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getId() {
    return id;
  }

  public void setId(BigDecimal id) {
    this.id = id;
  }

  public AttributesData name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public AttributesData sku(String sku) {
    this.sku = sku;
    return this;
  }

  /**
   * Get sku
   * @return sku
  **/
  @ApiModelProperty(value = "")


  public String getSku() {
    return sku;
  }

  public void setSku(String sku) {
    this.sku = sku;
  }

  public AttributesData price(String price) {
    this.price = price;
    return this;
  }

  /**
   * Get price
   * @return price
  **/
  @ApiModelProperty(value = "")


  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public AttributesData weight(String weight) {
    this.weight = weight;
    return this;
  }

  /**
   * Get weight
   * @return weight
  **/
  @ApiModelProperty(value = "")


  public String getWeight() {
    return weight;
  }

  public void setWeight(String weight) {
    this.weight = weight;
  }

  public AttributesData height(String height) {
    this.height = height;
    return this;
  }

  /**
   * Get height
   * @return height
  **/
  @ApiModelProperty(value = "")


  public String getHeight() {
    return height;
  }

  public void setHeight(String height) {
    this.height = height;
  }

  public AttributesData width(String width) {
    this.width = width;
    return this;
  }

  /**
   * Get width
   * @return width
  **/
  @ApiModelProperty(value = "")


  public String getWidth() {
    return width;
  }

  public void setWidth(String width) {
    this.width = width;
  }

  public AttributesData depth(String depth) {
    this.depth = depth;
    return this;
  }

  /**
   * Get depth
   * @return depth
  **/
  @ApiModelProperty(value = "")


  public String getDepth() {
    return depth;
  }

  public void setDepth(String depth) {
    this.depth = depth;
  }

  public AttributesData isMaster(Boolean isMaster) {
    this.isMaster = isMaster;
    return this;
  }

  /**
   * Get isMaster
   * @return isMaster
  **/
  @ApiModelProperty(value = "")


  public Boolean isIsMaster() {
    return isMaster;
  }

  public void setIsMaster(Boolean isMaster) {
    this.isMaster = isMaster;
  }

  public AttributesData slug(String slug) {
    this.slug = slug;
    return this;
  }

  /**
   * Get slug
   * @return slug
  **/
  @ApiModelProperty(value = "")


  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

  public AttributesData description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(value = "")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public AttributesData trackInventory(Boolean trackInventory) {
    this.trackInventory = trackInventory;
    return this;
  }

  /**
   * Get trackInventory
   * @return trackInventory
  **/
  @ApiModelProperty(value = "")


  public Boolean isTrackInventory() {
    return trackInventory;
  }

  public void setTrackInventory(Boolean trackInventory) {
    this.trackInventory = trackInventory;
  }

  public AttributesData displayPrice(String displayPrice) {
    this.displayPrice = displayPrice;
    return this;
  }

  /**
   * Get displayPrice
   * @return displayPrice
  **/
  @ApiModelProperty(value = "")


  public String getDisplayPrice() {
    return displayPrice;
  }

  public void setDisplayPrice(String displayPrice) {
    this.displayPrice = displayPrice;
  }

  public AttributesData optionsText(String optionsText) {
    this.optionsText = optionsText;
    return this;
  }

  /**
   * Get optionsText
   * @return optionsText
  **/
  @ApiModelProperty(value = "")


  public String getOptionsText() {
    return optionsText;
  }

  public void setOptionsText(String optionsText) {
    this.optionsText = optionsText;
  }

  public AttributesData inStock(Boolean inStock) {
    this.inStock = inStock;
    return this;
  }

  /**
   * Get inStock
   * @return inStock
  **/
  @ApiModelProperty(value = "")


  public Boolean isInStock() {
    return inStock;
  }

  public void setInStock(Boolean inStock) {
    this.inStock = inStock;
  }

  public AttributesData isBackorderable(Boolean isBackorderable) {
    this.isBackorderable = isBackorderable;
    return this;
  }

  /**
   * Get isBackorderable
   * @return isBackorderable
  **/
  @ApiModelProperty(value = "")


  public Boolean isIsBackorderable() {
    return isBackorderable;
  }

  public void setIsBackorderable(Boolean isBackorderable) {
    this.isBackorderable = isBackorderable;
  }

  public AttributesData totalOnHand(BigDecimal totalOnHand) {
    this.totalOnHand = totalOnHand;
    return this;
  }

  /**
   * Get totalOnHand
   * @return totalOnHand
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getTotalOnHand() {
    return totalOnHand;
  }

  public void setTotalOnHand(BigDecimal totalOnHand) {
    this.totalOnHand = totalOnHand;
  }

  public AttributesData isDestroyed(Boolean isDestroyed) {
    this.isDestroyed = isDestroyed;
    return this;
  }

  /**
   * Get isDestroyed
   * @return isDestroyed
  **/
  @ApiModelProperty(value = "")


  public Boolean isIsDestroyed() {
    return isDestroyed;
  }

  public void setIsDestroyed(Boolean isDestroyed) {
    this.isDestroyed = isDestroyed;
  }

  public AttributesData costPrice(String costPrice) {
    this.costPrice = costPrice;
    return this;
  }

  /**
   * Get costPrice
   * @return costPrice
  **/
  @ApiModelProperty(value = "")


  public String getCostPrice() {
    return costPrice;
  }

  public void setCostPrice(String costPrice) {
    this.costPrice = costPrice;
  }

  public Boolean isIsOrderable() {
    return isOrderable;
  }

  public void setOrderable(Boolean orderable) {
    isOrderable = orderable;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AttributesData attributesData = (AttributesData) o;
    return Objects.equals(this.id, attributesData.id) &&
        Objects.equals(this.name, attributesData.name) &&
        Objects.equals(this.sku, attributesData.sku) &&
        Objects.equals(this.price, attributesData.price) &&
        Objects.equals(this.weight, attributesData.weight) &&
        Objects.equals(this.height, attributesData.height) &&
        Objects.equals(this.width, attributesData.width) &&
        Objects.equals(this.depth, attributesData.depth) &&
        Objects.equals(this.isMaster, attributesData.isMaster) &&
        Objects.equals(this.slug, attributesData.slug) &&
        Objects.equals(this.description, attributesData.description) &&
        Objects.equals(this.trackInventory, attributesData.trackInventory) &&
        Objects.equals(this.displayPrice, attributesData.displayPrice) &&
        Objects.equals(this.optionsText, attributesData.optionsText) &&
        Objects.equals(this.inStock, attributesData.inStock) &&
        Objects.equals(this.isBackorderable, attributesData.isBackorderable) &&
        Objects.equals(this.totalOnHand, attributesData.totalOnHand) &&
        Objects.equals(this.isDestroyed, attributesData.isDestroyed) &&
        Objects.equals(this.isOrderable, attributesData.isOrderable) &&
        Objects.equals(this.costPrice, attributesData.costPrice);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, sku, price, weight, height, width, depth, isMaster, slug, description, trackInventory, displayPrice, optionsText, inStock, isBackorderable, totalOnHand, isDestroyed, costPrice);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AttributesData {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    sku: ").append(toIndentedString(sku)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("    weight: ").append(toIndentedString(weight)).append("\n");
    sb.append("    height: ").append(toIndentedString(height)).append("\n");
    sb.append("    width: ").append(toIndentedString(width)).append("\n");
    sb.append("    depth: ").append(toIndentedString(depth)).append("\n");
    sb.append("    isMaster: ").append(toIndentedString(isMaster)).append("\n");
    sb.append("    slug: ").append(toIndentedString(slug)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    trackInventory: ").append(toIndentedString(trackInventory)).append("\n");
    sb.append("    displayPrice: ").append(toIndentedString(displayPrice)).append("\n");
    sb.append("    optionsText: ").append(toIndentedString(optionsText)).append("\n");
    sb.append("    inStock: ").append(toIndentedString(inStock)).append("\n");
    sb.append("    isBackorderable: ").append(toIndentedString(isBackorderable)).append("\n");
    sb.append("    isOrderable: ").append(toIndentedString(isOrderable)).append("\n");
    sb.append("    totalOnHand: ").append(toIndentedString(totalOnHand)).append("\n");
    sb.append("    isDestroyed: ").append(toIndentedString(isDestroyed)).append("\n");
    sb.append("    costPrice: ").append(toIndentedString(costPrice)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

