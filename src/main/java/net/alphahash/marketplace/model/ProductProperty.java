package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ProductProperty
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

public class ProductProperty   {
  @JsonProperty("id")
  private BigDecimal id = null;

  @JsonProperty("product_id")
  private BigDecimal productId = null;

  @JsonProperty("property_id")
  private BigDecimal propertyId = null;

  @JsonProperty("value")
  private String value = null;

  @JsonProperty("property_name")
  private String propertyName = null;

  public ProductProperty id(BigDecimal id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getId() {
    return id;
  }

  public void setId(BigDecimal id) {
    this.id = id;
  }

  public ProductProperty productId(BigDecimal productId) {
    this.productId = productId;
    return this;
  }

  /**
   * Get productId
   * @return productId
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getProductId() {
    return productId;
  }

  public void setProductId(BigDecimal productId) {
    this.productId = productId;
  }

  public ProductProperty propertyId(BigDecimal propertyId) {
    this.propertyId = propertyId;
    return this;
  }

  /**
   * Get propertyId
   * @return propertyId
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getPropertyId() {
    return propertyId;
  }

  public void setPropertyId(BigDecimal propertyId) {
    this.propertyId = propertyId;
  }

  public ProductProperty value(String value) {
    this.value = value;
    return this;
  }

  /**
   * Get value
   * @return value
  **/
  @ApiModelProperty(value = "")


  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public ProductProperty propertyName(String propertyName) {
    this.propertyName = propertyName;
    return this;
  }

  /**
   * Get propertyName
   * @return propertyName
  **/
  @ApiModelProperty(value = "")


  public String getPropertyName() {
    return propertyName;
  }

  public void setPropertyName(String propertyName) {
    this.propertyName = propertyName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProductProperty productProperty = (ProductProperty) o;
    return Objects.equals(this.id, productProperty.id) &&
        Objects.equals(this.productId, productProperty.productId) &&
        Objects.equals(this.propertyId, productProperty.propertyId) &&
        Objects.equals(this.value, productProperty.value) &&
        Objects.equals(this.propertyName, productProperty.propertyName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, productId, propertyId, value, propertyName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProductProperty {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    productId: ").append(toIndentedString(productId)).append("\n");
    sb.append("    propertyId: ").append(toIndentedString(propertyId)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("    propertyName: ").append(toIndentedString(propertyName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

