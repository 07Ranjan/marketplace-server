package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import net.alphahash.marketplace.model.TaxonomiesInnerBody;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Taxonomies
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

public class Taxonomies   {
  @JsonProperty("taxonomies")
  @Valid
  private List<TaxonomiesInnerBody> taxonomies = null;

  @JsonProperty("count")
  private Integer count = null;

  @JsonProperty("current_page")
  private Integer currentPage = null;

  @JsonProperty("pages")
  private Integer pages = null;

  public Taxonomies taxonomies(List<TaxonomiesInnerBody> taxonomies) {
    this.taxonomies = taxonomies;
    return this;
  }

  public Taxonomies addTaxonomiesItem(TaxonomiesInnerBody taxonomiesItem) {
    if (this.taxonomies == null) {
      this.taxonomies = new ArrayList<TaxonomiesInnerBody>();
    }
    this.taxonomies.add(taxonomiesItem);
    return this;
  }

  /**
   * Get taxonomies
   * @return taxonomies
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<TaxonomiesInnerBody> getTaxonomies() {
    return taxonomies;
  }

  public void setTaxonomies(List<TaxonomiesInnerBody> taxonomies) {
    this.taxonomies = taxonomies;
  }

  public Taxonomies count(Integer count) {
    this.count = count;
    return this;
  }

  /**
   * Get count
   * @return count
  **/
  @ApiModelProperty(value = "")


  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public Taxonomies currentPage(Integer currentPage) {
    this.currentPage = currentPage;
    return this;
  }

  /**
   * Get currentPage
   * @return currentPage
  **/
  @ApiModelProperty(value = "")


  public Integer getCurrentPage() {
    return currentPage;
  }

  public void setCurrentPage(Integer currentPage) {
    this.currentPage = currentPage;
  }

  public Taxonomies pages(Integer pages) {
    this.pages = pages;
    return this;
  }

  /**
   * Get pages
   * @return pages
  **/
  @ApiModelProperty(value = "")


  public Integer getPages() {
    return pages;
  }

  public void setPages(Integer pages) {
    this.pages = pages;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Taxonomies taxonomies = (Taxonomies) o;
    return Objects.equals(this.taxonomies, taxonomies.taxonomies) &&
        Objects.equals(this.count, taxonomies.count) &&
        Objects.equals(this.currentPage, taxonomies.currentPage) &&
        Objects.equals(this.pages, taxonomies.pages);
  }

  @Override
  public int hashCode() {
    return Objects.hash(taxonomies, count, currentPage, pages);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Taxonomies {\n");
    
    sb.append("    taxonomies: ").append(toIndentedString(taxonomies)).append("\n");
    sb.append("    count: ").append(toIndentedString(count)).append("\n");
    sb.append("    currentPage: ").append(toIndentedString(currentPage)).append("\n");
    sb.append("    pages: ").append(toIndentedString(pages)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

