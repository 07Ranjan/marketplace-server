package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import net.alphahash.marketplace.model.Image;
import net.alphahash.marketplace.model.OptionValue;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Variant
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

public class Variant   {
  @JsonProperty("id")
  private BigDecimal id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("sku")
  private String sku = null;

  @JsonProperty("price")
  private String price = null;

  @JsonProperty("weight")
  private String weight = null;

  @JsonProperty("height")
  private String height = null;

  @JsonProperty("width")
  private String width = null;

  @JsonProperty("depth")
  private String depth = null;

  @JsonProperty("is_master")
  private Boolean isMaster = null;

  @JsonProperty("slug")
  private String slug = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("track_inventory")
  private Boolean trackInventory = null;

  @JsonProperty("cost_price")
  private String costPrice = null;

  @JsonProperty("option_values")
  @Valid
  private List<OptionValue> optionValues = null;

  @JsonProperty("total_on_hand")
  private BigDecimal totalOnHand = null;

  @JsonProperty("display_price")
  private String displayPrice = null;

  @JsonProperty("options_text")
  private String optionsText = null;

  @JsonProperty("in_stock")
  private Boolean inStock = null;

  @JsonProperty("is_backorderable")
  private Boolean isBackorderable = null;

  @JsonProperty("is_destroyed")
  private Boolean isDestroyed = null;

  @JsonProperty("is_orderable")
  private Boolean isOrderable = null;

  @JsonProperty("images")
  @Valid
  private List<Image> images = null;

  public Variant id(BigDecimal id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getId() {
    return id;
  }

  public void setId(BigDecimal id) {
    this.id = id;
  }

  public Variant name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Variant sku(String sku) {
    this.sku = sku;
    return this;
  }

  /**
   * Get sku
   * @return sku
  **/
  @ApiModelProperty(value = "")


  public String getSku() {
    return sku;
  }

  public void setSku(String sku) {
    this.sku = sku;
  }

  public Variant price(String price) {
    this.price = price;
    return this;
  }

  /**
   * Get price
   * @return price
  **/
  @ApiModelProperty(value = "")


  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public Variant weight(String weight) {
    this.weight = weight;
    return this;
  }

  /**
   * Get weight
   * @return weight
  **/
  @ApiModelProperty(value = "")


  public String getWeight() {
    return weight;
  }

  public void setWeight(String weight) {
    this.weight = weight;
  }

  public Variant height(String height) {
    this.height = height;
    return this;
  }

  /**
   * Get height
   * @return height
  **/
  @ApiModelProperty(value = "")


  public String getHeight() {
    return height;
  }

  public void setHeight(String height) {
    this.height = height;
  }

  public Variant width(String width) {
    this.width = width;
    return this;
  }

  /**
   * Get width
   * @return width
  **/
  @ApiModelProperty(value = "")


  public String getWidth() {
    return width;
  }

  public void setWidth(String width) {
    this.width = width;
  }

  public Variant depth(String depth) {
    this.depth = depth;
    return this;
  }

  /**
   * Get depth
   * @return depth
  **/
  @ApiModelProperty(value = "")


  public String getDepth() {
    return depth;
  }

  public void setDepth(String depth) {
    this.depth = depth;
  }

  public Variant isMaster(Boolean isMaster) {
    this.isMaster = isMaster;
    return this;
  }

  /**
   * Get isMaster
   * @return isMaster
  **/
  @ApiModelProperty(value = "")


  public Boolean isIsMaster() {
    return isMaster;
  }

  public void setIsMaster(Boolean isMaster) {
    this.isMaster = isMaster;
  }

  public Variant slug(String slug) {
    this.slug = slug;
    return this;
  }

  /**
   * Get slug
   * @return slug
  **/
  @ApiModelProperty(value = "")


  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

  public Variant description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(value = "")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Variant trackInventory(Boolean trackInventory) {
    this.trackInventory = trackInventory;
    return this;
  }

  /**
   * Get trackInventory
   * @return trackInventory
  **/
  @ApiModelProperty(value = "")


  public Boolean isTrackInventory() {
    return trackInventory;
  }

  public void setTrackInventory(Boolean trackInventory) {
    this.trackInventory = trackInventory;
  }

  public Variant costPrice(String costPrice) {
    this.costPrice = costPrice;
    return this;
  }

  /**
   * Get costPrice
   * @return costPrice
  **/
  @ApiModelProperty(value = "")


  public String getCostPrice() {
    return costPrice;
  }

  public void setCostPrice(String costPrice) {
    this.costPrice = costPrice;
  }

  public Variant optionValues(List<OptionValue> optionValues) {
    this.optionValues = optionValues;
    return this;
  }

  public Variant addOptionValuesItem(OptionValue optionValuesItem) {
    if (this.optionValues == null) {
      this.optionValues = new ArrayList<OptionValue>();
    }
    this.optionValues.add(optionValuesItem);
    return this;
  }

  /**
   * Get optionValues
   * @return optionValues
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<OptionValue> getOptionValues() {
    return optionValues;
  }

  public void setOptionValues(List<OptionValue> optionValues) {
    this.optionValues = optionValues;
  }

  public Variant totalOnHand(BigDecimal totalOnHand) {
    this.totalOnHand = totalOnHand;
    return this;
  }

  /**
   * Get totalOnHand
   * @return totalOnHand
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getTotalOnHand() {
    return totalOnHand;
  }

  public void setTotalOnHand(BigDecimal totalOnHand) {
    this.totalOnHand = totalOnHand;
  }

  public Variant displayPrice(String displayPrice) {
    this.displayPrice = displayPrice;
    return this;
  }

  /**
   * Get displayPrice
   * @return displayPrice
  **/
  @ApiModelProperty(value = "")


  public String getDisplayPrice() {
    return displayPrice;
  }

  public void setDisplayPrice(String displayPrice) {
    this.displayPrice = displayPrice;
  }

  public Variant optionsText(String optionsText) {
    this.optionsText = optionsText;
    return this;
  }

  /**
   * Get optionsText
   * @return optionsText
  **/
  @ApiModelProperty(value = "")


  public String getOptionsText() {
    return optionsText;
  }

  public void setOptionsText(String optionsText) {
    this.optionsText = optionsText;
  }

  public Variant inStock(Boolean inStock) {
    this.inStock = inStock;
    return this;
  }

  /**
   * Get inStock
   * @return inStock
  **/
  @ApiModelProperty(value = "")


  public Boolean isInStock() {
    return inStock;
  }

  public void setInStock(Boolean inStock) {
    this.inStock = inStock;
  }

  public Variant isBackorderable(Boolean isBackorderable) {
    this.isBackorderable = isBackorderable;
    return this;
  }

  /**
   * Get isBackorderable
   * @return isBackorderable
  **/
  @ApiModelProperty(value = "")


  public Boolean isIsBackorderable() {
    return isBackorderable;
  }

  public void setIsBackorderable(Boolean isBackorderable) {
    this.isBackorderable = isBackorderable;
  }

  public Variant isDestroyed(Boolean isDestroyed) {
    this.isDestroyed = isDestroyed;
    return this;
  }

  /**
   * Get isDestroyed
   * @return isDestroyed
  **/
  @ApiModelProperty(value = "")


  public Boolean isIsDestroyed() {
    return isDestroyed;
  }

  public void setIsDestroyed(Boolean isDestroyed) {
    this.isDestroyed = isDestroyed;
  }

  public Variant isOrderable(Boolean isOrderable) {
    this.isOrderable = isOrderable;
    return this;
  }

  /**
   * Get isOrderable
   * @return isOrderable
  **/
  @ApiModelProperty(value = "")


  public Boolean isIsOrderable() {
    return isOrderable;
  }

  public void setIsOrderable(Boolean isOrderable) {
    this.isOrderable = isOrderable;
  }

  public Variant images(List<Image> images) {
    this.images = images;
    return this;
  }

  public Variant addImagesItem(Image imagesItem) {
    if (this.images == null) {
      this.images = new ArrayList<Image>();
    }
    this.images.add(imagesItem);
    return this;
  }

  /**
   * Get images
   * @return images
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Image> getImages() {
    return images;
  }

  public void setImages(List<Image> images) {
    this.images = images;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Variant variant = (Variant) o;
    return Objects.equals(this.id, variant.id) &&
        Objects.equals(this.name, variant.name) &&
        Objects.equals(this.sku, variant.sku) &&
        Objects.equals(this.price, variant.price) &&
        Objects.equals(this.weight, variant.weight) &&
        Objects.equals(this.height, variant.height) &&
        Objects.equals(this.width, variant.width) &&
        Objects.equals(this.depth, variant.depth) &&
        Objects.equals(this.isMaster, variant.isMaster) &&
        Objects.equals(this.slug, variant.slug) &&
        Objects.equals(this.description, variant.description) &&
        Objects.equals(this.trackInventory, variant.trackInventory) &&
        Objects.equals(this.costPrice, variant.costPrice) &&
        Objects.equals(this.optionValues, variant.optionValues) &&
        Objects.equals(this.totalOnHand, variant.totalOnHand) &&
        Objects.equals(this.displayPrice, variant.displayPrice) &&
        Objects.equals(this.optionsText, variant.optionsText) &&
        Objects.equals(this.inStock, variant.inStock) &&
        Objects.equals(this.isBackorderable, variant.isBackorderable) &&
        Objects.equals(this.isDestroyed, variant.isDestroyed) &&
        Objects.equals(this.isOrderable, variant.isOrderable) &&
        Objects.equals(this.images, variant.images);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, sku, price, weight, height, width, depth, isMaster, slug, description, trackInventory, costPrice, optionValues, totalOnHand, displayPrice, optionsText, inStock, isBackorderable, isDestroyed, isOrderable, images);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Variant {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    sku: ").append(toIndentedString(sku)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("    weight: ").append(toIndentedString(weight)).append("\n");
    sb.append("    height: ").append(toIndentedString(height)).append("\n");
    sb.append("    width: ").append(toIndentedString(width)).append("\n");
    sb.append("    depth: ").append(toIndentedString(depth)).append("\n");
    sb.append("    isMaster: ").append(toIndentedString(isMaster)).append("\n");
    sb.append("    slug: ").append(toIndentedString(slug)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    trackInventory: ").append(toIndentedString(trackInventory)).append("\n");
    sb.append("    costPrice: ").append(toIndentedString(costPrice)).append("\n");
    sb.append("    optionValues: ").append(toIndentedString(optionValues)).append("\n");
    sb.append("    totalOnHand: ").append(toIndentedString(totalOnHand)).append("\n");
    sb.append("    displayPrice: ").append(toIndentedString(displayPrice)).append("\n");
    sb.append("    optionsText: ").append(toIndentedString(optionsText)).append("\n");
    sb.append("    inStock: ").append(toIndentedString(inStock)).append("\n");
    sb.append("    isBackorderable: ").append(toIndentedString(isBackorderable)).append("\n");
    sb.append("    isDestroyed: ").append(toIndentedString(isDestroyed)).append("\n");
    sb.append("    isOrderable: ").append(toIndentedString(isOrderable)).append("\n");
    sb.append("    images: ").append(toIndentedString(images)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

