package net.alphahash.marketplace.model;

import java.util.List;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import net.alphahash.marketplace.model.Variant;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * LineItem
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

public class LineItem   {
  @JsonProperty("id")
  private BigDecimal id = null;

  @JsonProperty("quantity")
  private BigDecimal quantity = null;

  @JsonProperty("price")
  private String price = null;

  @JsonProperty("single_display_amount")
  private String singleDisplayAmount = null;

  @JsonProperty("total")
  private String total = null;

  @JsonProperty("display_amount")
  private String displayAmount = null;

  @JsonProperty("adjustments")
  private List<String> adjustments = null;

  @JsonProperty("variant_id")
  private BigDecimal variantId = null;

  @JsonProperty("variant")
  private Variant variant = null;

  public LineItem id(BigDecimal id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getId() {
    return id;
  }

  public void setId(BigDecimal id) {
    this.id = id;
  }

  public LineItem quantity(BigDecimal quantity) {
    this.quantity = quantity;
    return this;
  }

  /**
   * Get quantity
   * @return quantity
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public LineItem price(String price) {
    this.price = price;
    return this;
  }

  /**
   * Get price
   * @return price
  **/
  @ApiModelProperty(value = "")

  @Valid

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public LineItem singleDisplayAmount(String singleDisplayAmount) {
    this.singleDisplayAmount = singleDisplayAmount;
    return this;
  }

  /**
   * Get singleDisplayAmount
   * @return singleDisplayAmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public String getSingleDisplayAmount() {
    return singleDisplayAmount;
  }

  public void setSingleDisplayAmount(String singleDisplayAmount) {
    this.singleDisplayAmount = singleDisplayAmount;
  }

  public LineItem total(String total) {
    this.total = total;
    return this;
  }

  /**
   * Get total
   * @return total
  **/
  @ApiModelProperty(value = "")

  @Valid

  public String getTotal() {
    return total;
  }

  public void setTotal(String total) {
    this.total = total;
  }

  public LineItem displayAmount(String displayAmount) {
    this.displayAmount = displayAmount;
    return this;
  }

  /**
   * Get displayAmount
   * @return displayAmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public String getDisplayAmount() {
    return displayAmount;
  }

  public void setDisplayAmount(String displayAmount) {
    this.displayAmount = displayAmount;
  }

  public LineItem variantId(BigDecimal variantId) {
    this.variantId = variantId;
    return this;
  }

  /**
   * Get variantId
   * @return variantId
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getVariantId() {
    return variantId;
  }

  public void setVariantId(BigDecimal variantId) {
    this.variantId = variantId;
  }

  public LineItem variant(Variant variant) {
    this.variant = variant;
    return this;
  }

  /**
   * Get variant
   * @return variant
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Variant getVariant() {
    return variant;
  }

  public void setVariant(Variant variant) {
    this.variant = variant;
  }

  public List<String> getAdjustments() {
    return adjustments;
  }

  public void setAdjustments(List<String> adjustments) {
    this.adjustments = adjustments;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LineItem lineItem = (LineItem) o;
    return Objects.equals(this.id, lineItem.id) &&
        Objects.equals(this.quantity, lineItem.quantity) &&
        Objects.equals(this.price, lineItem.price) &&
        Objects.equals(this.singleDisplayAmount, lineItem.singleDisplayAmount) &&
        Objects.equals(this.total, lineItem.total) &&
        Objects.equals(this.displayAmount, lineItem.displayAmount) &&
        Objects.equals(this.variantId, lineItem.variantId) &&
        Objects.equals(this.adjustments, lineItem.adjustments) &&
        Objects.equals(this.variant, lineItem.variant);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, quantity, price, singleDisplayAmount, total, displayAmount, variantId, variant);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LineItem {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("    singleDisplayAmount: ").append(toIndentedString(singleDisplayAmount)).append("\n");
    sb.append("    total: ").append(toIndentedString(total)).append("\n");
    sb.append("    displayAmount: ").append(toIndentedString(displayAmount)).append("\n");
    sb.append("    variantId: ").append(toIndentedString(variantId)).append("\n");
    sb.append("    variant: ").append(toIndentedString(variant)).append("\n");
    sb.append("    adjustments: ").append(toIndentedString(adjustments)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

