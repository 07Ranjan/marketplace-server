package net.alphahash.marketplace.model;


import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Country
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-09-02T23:09:02.759+01:00")

public class Country   {
    @JsonProperty("id")
    private String id = null;

    @JsonProperty("iso_name")
    private String isoName = null;

    @JsonProperty("iso")
    private String iso = null;

    @JsonProperty("iso3")
    private String iso3 = null;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("numcode")
    private BigDecimal numcode = null;

    public Country id(String id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     * @return id
     **/
    @ApiModelProperty(value = "")


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Country isoName(String isoName) {
        this.isoName = isoName;
        return this;
    }

    /**
     * Get isoName
     * @return isoName
     **/
    @ApiModelProperty(value = "")


    public String getIsoName() {
        return isoName;
    }

    public void setIsoName(String isoName) {
        this.isoName = isoName;
    }

    public Country iso(String iso) {
        this.iso = iso;
        return this;
    }

    /**
     * Get iso
     * @return iso
     **/
    @ApiModelProperty(value = "")


    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public Country iso3(String iso3) {
        this.iso3 = iso3;
        return this;
    }

    /**
     * Get iso3
     * @return iso3
     **/
    @ApiModelProperty(value = "")


    public String getIso3() {
        return iso3;
    }

    public void setIso3(String iso3) {
        this.iso3 = iso3;
    }

    public Country name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     * @return name
     **/
    @ApiModelProperty(value = "")


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country numcode(BigDecimal numcode) {
        this.numcode = numcode;
        return this;
    }

    /**
     * Get numcode
     * @return numcode
     **/
    @ApiModelProperty(value = "")

    @Valid

    public BigDecimal getNumcode() {
        return numcode;
    }

    public void setNumcode(BigDecimal numcode) {
        this.numcode = numcode;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Country country = (Country) o;
        return Objects.equals(this.id, country.id) &&
                Objects.equals(this.isoName, country.isoName) &&
                Objects.equals(this.iso, country.iso) &&
                Objects.equals(this.iso3, country.iso3) &&
                Objects.equals(this.name, country.name) &&
                Objects.equals(this.numcode, country.numcode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, isoName, iso, iso3, name, numcode);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Country {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    isoName: ").append(toIndentedString(isoName)).append("\n");
        sb.append("    iso: ").append(toIndentedString(iso)).append("\n");
        sb.append("    iso3: ").append(toIndentedString(iso3)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    numcode: ").append(toIndentedString(numcode)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

