package net.alphahash.marketplace.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * PaymentDetailsRequest
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

public class PaymentDetailsRequest   {
  @JsonProperty("user_id")
  private String userId = null;

  @JsonProperty("tokenised")
  private Boolean tokenised = null;

  @JsonProperty("number")
  private String number = null;

  @JsonProperty("exp_month")
  private Integer expMonth = null;

  @JsonProperty("exp_year")
  private Integer expYear = null;

  @JsonProperty("cvc")
  private Integer cvc = null;

  @JsonProperty("amount")
  private Long amount = null;

  public PaymentDetailsRequest userId(String userId) {
    this.userId = userId;
    return this;
  }

  /**
   * Get userId
   * @return userId
  **/
  @ApiModelProperty(value = "")


  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public PaymentDetailsRequest tokenised(Boolean tokenised) {
    this.tokenised = tokenised;
    return this;
  }

  /**
   * Get tokenised
   * @return tokenised
  **/
  @ApiModelProperty(value = "")


  public Boolean isTokenised() {
    return tokenised;
  }

  public void setTokenised(Boolean tokenised) {
    this.tokenised = tokenised;
  }

  public PaymentDetailsRequest number(String number) {
    this.number = number;
    return this;
  }

  /**
   * Get number
   * @return number
  **/
  @ApiModelProperty(value = "")


  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public PaymentDetailsRequest expMonth(Integer expMonth) {
    this.expMonth = expMonth;
    return this;
  }

  /**
   * Get expMonth
   * @return expMonth
  **/
  @ApiModelProperty(value = "")


  public Integer getExpMonth() {
    return expMonth;
  }

  public void setExpMonth(Integer expMonth) {
    this.expMonth = expMonth;
  }

  public PaymentDetailsRequest expYear(Integer expYear) {
    this.expYear = expYear;
    return this;
  }

  /**
   * Get expYear
   * @return expYear
  **/
  @ApiModelProperty(value = "")


  public Integer getExpYear() {
    return expYear;
  }

  public void setExpYear(Integer expYear) {
    this.expYear = expYear;
  }

  public PaymentDetailsRequest cvc(Integer cvc) {
    this.cvc = cvc;
    return this;
  }

  /**
   * Get cvc
   * @return cvc
  **/
  @ApiModelProperty(value = "")


  public Integer getCvc() {
    return cvc;
  }

  public void setCvc(Integer cvc) {
    this.cvc = cvc;
  }

  public PaymentDetailsRequest amount(Long amount) {
    this.amount = amount;
    return this;
  }

  /**
   * Get amount
   * @return amount
  **/
  @ApiModelProperty(value = "")


  public Long getAmount() {
    return amount;
  }

  public void setAmount(Long amount) {
    this.amount = amount;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentDetailsRequest paymentDetailsRequest = (PaymentDetailsRequest) o;
    return Objects.equals(this.userId, paymentDetailsRequest.userId) &&
        Objects.equals(this.tokenised, paymentDetailsRequest.tokenised) &&
        Objects.equals(this.number, paymentDetailsRequest.number) &&
        Objects.equals(this.expMonth, paymentDetailsRequest.expMonth) &&
        Objects.equals(this.expYear, paymentDetailsRequest.expYear) &&
        Objects.equals(this.cvc, paymentDetailsRequest.cvc) &&
        Objects.equals(this.amount, paymentDetailsRequest.amount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, tokenised, number, expMonth, expYear, cvc, amount);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentDetailsRequest {\n");
    
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    tokenised: ").append(toIndentedString(tokenised)).append("\n");
    sb.append("    number: ").append(toIndentedString(number)).append("\n");
    sb.append("    expMonth: ").append(toIndentedString(expMonth)).append("\n");
    sb.append("    expYear: ").append(toIndentedString(expYear)).append("\n");
    sb.append("    cvc: ").append(toIndentedString(cvc)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

