package net.alphahash.marketplace.api;

import net.alphahash.marketplace.model.ProductDetails;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

@Controller
public class TaxonsApiController implements TaxonsApi {

    private static final Logger log = LoggerFactory.getLogger(TaxonsApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public TaxonsApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<ProductDetails> taxonsProductsGet(@ApiParam(value = "taxon id.") @Valid @RequestParam(value = "id", required = false) String id,@ApiParam(value = "The number of products to return per page.") @Valid @RequestParam(value = "per_page", required = false) String perPage,@ApiParam(value = "size of data set.") @Valid @RequestParam(value = "data_set", required = false) String dataSet) {
            try {
                return new ResponseEntity<ProductDetails>(objectMapper.readValue("{\"pagination\":{\"count\":1,\"total_count\":1,\"current_page\":1,\"per_page\":20,\"pages\":1},\"data\":[{\"id\":17,\"type\":\"products\",\"attributes\":{\"slug\":\"ekebol-wooden-steal\",\"name\":\"Sofa, Katorp natural\",\"product_url\":\"https://ngspree-api.herokuapp.com/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBTUT09IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--4c8261617f85badcfec9dd807f0bc47a36ea04ab/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lOTWpRd2VESTBNRDRHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--abf3638afc8618aef00ed16272a3ccf7341c3ee1/EKEBOL-wooden-1.JPG\",\"price\":\"399.0\",\"cost_price\":\"420.0\",\"avg_rating\":\"0.0\",\"reviews_count\":0,\"currency\":\"USD\",\"currency_symbol\":\"$\"}}]}", ProductDetails.class), HttpStatus.OK);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<ProductDetails>(HttpStatus.INTERNAL_SERVER_ERROR);
            }

    }

}
