package net.alphahash.marketplace.api;

import net.alphahash.marketplace.model.States;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

@Controller
public class CountriesApiController implements CountriesApi {

    private static final Logger log = LoggerFactory.getLogger(CountriesApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public CountriesApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<States> countries() {

            try {
                return new ResponseEntity<States>(objectMapper.readValue("{\"states\":[{\"id\":1125,\"name\":\"Andaman and Nicobar Islands\",\"abbr\":\"AN\",\"country_id\":105},{\"id\":1126,\"name\":\"Andhra Pradesh\",\"abbr\":\"AP\",\"country_id\":105},{\"id\":1127,\"name\":\"Arunachal Pradesh\",\"abbr\":\"AR\",\"country_id\":105},{\"id\":1128,\"name\":\"Assam\",\"abbr\":\"AS\",\"country_id\":105},{\"id\":1129,\"name\":\"Bihar\",\"abbr\":\"BR\",\"country_id\":105},{\"id\":1130,\"name\":\"Chandigarh\",\"abbr\":\"CH\",\"country_id\":105},{\"id\":1131,\"name\":\"Chhattisgarh\",\"abbr\":\"CT\",\"country_id\":105},{\"id\":1134,\"name\":\"Dadra and Nagar Haveli\",\"abbr\":\"DN\",\"country_id\":105},{\"id\":1132,\"name\":\"Damen and Diu\",\"abbr\":\"DD\",\"country_id\":105},{\"id\":1133,\"name\":\"Delhi\",\"abbr\":\"DL\",\"country_id\":105},{\"id\":1135,\"name\":\"Goa\",\"abbr\":\"GA\",\"country_id\":105},{\"id\":1136,\"name\":\"Gujarat\",\"abbr\":\"GJ\",\"country_id\":105},{\"id\":1138,\"name\":\"Haryana\",\"abbr\":\"HR\",\"country_id\":105},{\"id\":1137,\"name\":\"Himachal Pradesh\",\"abbr\":\"HP\",\"country_id\":105},{\"id\":1140,\"name\":\"Jammu and Kashmir\",\"abbr\":\"JK\",\"country_id\":105},{\"id\":1139,\"name\":\"Jharkhand\",\"abbr\":\"JH\",\"country_id\":105},{\"id\":1141,\"name\":\"Karnataka\",\"abbr\":\"KA\",\"country_id\":105},{\"id\":1142,\"name\":\"Kerala\",\"abbr\":\"KL\",\"country_id\":105},{\"id\":1143,\"name\":\"Lakshadweep\",\"abbr\":\"LD\",\"country_id\":105},{\"id\":1147,\"name\":\"Madhya Pradesh\",\"abbr\":\"MP\",\"country_id\":105},{\"id\":1144,\"name\":\"Maharashtra\",\"abbr\":\"MH\",\"country_id\":105},{\"id\":1146,\"name\":\"Manipur\",\"abbr\":\"MN\",\"country_id\":105},{\"id\":1145,\"name\":\"Meghalaya\",\"abbr\":\"ML\",\"country_id\":105},{\"id\":1148,\"name\":\"Mizoram\",\"abbr\":\"MZ\",\"country_id\":105},{\"id\":1149,\"name\":\"Nagaland\",\"abbr\":\"NL\",\"country_id\":105},{\"id\":1150,\"name\":\"Orissa\",\"abbr\":\"OR\",\"country_id\":105},{\"id\":1152,\"name\":\"Puducherry\",\"abbr\":\"PY\",\"country_id\":105},{\"id\":1151,\"name\":\"Punjab\",\"abbr\":\"PB\",\"country_id\":105},{\"id\":1153,\"name\":\"Rajasthan\",\"abbr\":\"RJ\",\"country_id\":105},{\"id\":1154,\"name\":\"Sikkim\",\"abbr\":\"SK\",\"country_id\":105},{\"id\":1155,\"name\":\"Tamil Nadu\",\"abbr\":\"TN\",\"country_id\":105},{\"id\":1156,\"name\":\"Tripura\",\"abbr\":\"TR\",\"country_id\":105},{\"id\":1158,\"name\":\"Uttarakhand\",\"abbr\":\"UT\",\"country_id\":105},{\"id\":1157,\"name\":\"Uttar Pradesh\",\"abbr\":\"UP\",\"country_id\":105},{\"id\":1159,\"name\":\"West Bengal\",\"abbr\":\"WB\",\"country_id\":105}]}", States.class), HttpStatus.OK);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<States>(HttpStatus.INTERNAL_SERVER_ERROR);
            }

    }

}
