package net.alphahash.marketplace.api;

import net.alphahash.marketplace.model.ProductDetails;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

@Controller
public class FavoriteProductsJsonApiController implements FavoriteProductsJsonApi {

    private static final Logger log = LoggerFactory.getLogger(FavoriteProductsJsonApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public FavoriteProductsJsonApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<ProductDetails> listFavoriteProducts(@ApiParam(value = "The number of favorite products to return per page.") @Valid @RequestParam(value = "per_page", required = false) String perPage,@ApiParam(value = "size of favorite products.") @Valid @RequestParam(value = "data_set", required = false) String dataSet) {

            try {
                return new ResponseEntity<ProductDetails>(objectMapper.readValue("{\"pagination\":{\"count\":3,\"total_count\":3,\"current_page\":1,\"per_page\":20,\"pages\":1},\"data\":[{\"id\":19,\"type\":\"products\",\"attributes\":{\"slug\":\"cushion\",\"name\":\"Cushion\",\"product_url\":\"https://ngspree-api.herokuapp.com/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBUZz09IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--103ef9f8c4d9082c4bb2e6ebfe6331fccffb3c92/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lOTWpRd2VESTBNRDRHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--abf3638afc8618aef00ed16272a3ccf7341c3ee1/nedja-cushion-multicolour1.jpg\",\"price\":\"5.3\",\"cost_price\":\"6.5\",\"avg_rating\":\"0.0\",\"reviews_count\":0,\"currency\":\"$\"}},{\"id\":21,\"type\":\"products\",\"attributes\":{\"slug\":\"cot-wooden-kids-bed\",\"name\":\"Cot Wooden kids Bed\",\"product_url\":\"https://ngspree-api.herokuapp.com/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBVUT09IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--12d625f19ac66010431e2d0ac93b50a3ec2779d5/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lOTWpRd2VESTBNRDRHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--abf3638afc8618aef00ed16272a3ccf7341c3ee1/www.maxpixel.net-Cradle-Digital-Art-Isolated-Cot-Wooden-Bed-Bed-1786088.png\",\"price\":\"120.0\",\"cost_price\":\"135.0\",\"avg_rating\":\"0.0\",\"reviews_count\":0,\"currency\":\"$\"}},{\"id\":17,\"type\":\"products\",\"attributes\":{\"slug\":\"ekebol-wooden-steal\",\"name\":\"Sofa, Katorp natural\",\"product_url\":\"https://ngspree-api.herokuapp.com/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBTUT09IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--4c8261617f85badcfec9dd807f0bc47a36ea04ab/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCam9MY21WemFYcGxTU0lOTWpRd2VESTBNRDRHT2daRlZBPT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--abf3638afc8618aef00ed16272a3ccf7341c3ee1/EKEBOL-wooden-1.JPG\",\"price\":\"399.0\",\"cost_price\":\"420.0\",\"avg_rating\":\"0.0\",\"reviews_count\":0,\"currency\":\"$\"}}]}", ProductDetails.class), HttpStatus.OK);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<ProductDetails>(HttpStatus.INTERNAL_SERVER_ERROR);
            }

    }

}
