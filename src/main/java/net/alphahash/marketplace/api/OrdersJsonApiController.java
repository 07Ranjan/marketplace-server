package net.alphahash.marketplace.api;

import net.alphahash.marketplace.model.Order;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-29T16:08:40.629+01:00")

@Controller
public class OrdersJsonApiController implements OrdersJsonApi {

    private static final Logger log = LoggerFactory.getLogger(OrdersJsonApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public OrdersJsonApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Order> ordersJsonPost() {

            try {
                return new ResponseEntity<Order>(objectMapper.readValue("{\"id\":4962,\"number\":\"R584678854\",\"item_total\":\"0.0\",\"total\":\"0.0\",\"ship_total\":\"0.0\",\"state\":\"cart\",\"adjustment_total\":\"0.0\",\"user_id\":null,\"created_at\":\"2018-07-16T18:37:31.148Z\",\"updated_at\":\"2018-07-16T18:37:31.323Z\",\"completed_at\":null,\"payment_total\":\"0.0\",\"shipment_state\":null,\"payment_state\":null,\"email\":null,\"special_instructions\":null,\"channel\":\"spree\",\"included_tax_total\":\"0.0\",\"additional_tax_total\":\"0.0\",\"display_included_tax_total\":\"$0.00\",\"display_additional_tax_total\":\"$0.00\",\"tax_total\":\"0.0\",\"currency\":\"USD\",\"considered_risky\":false,\"canceler_id\":null,\"display_item_total\":\"$0.00\",\"total_quantity\":0,\"display_total\":\"$0.00\",\"display_ship_total\":\"$0.00\",\"display_tax_total\":\"$0.00\",\"display_adjustment_total\":\"$0.00\",\"token\":\"9tW3iLmDF0ju9mfTd18jGQ1531766251148\",\"checkout_steps\":[\"address\",\"delivery\",\"complete\"],\"bill_address\":null,\"ship_address\":null,\"line_items\":[],\"payments\":[],\"shipments\":null,\"adjustments\":null,\"credit_cards\":null,\"permissions\":true}}", Order.class), HttpStatus.OK);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Order>(HttpStatus.INTERNAL_SERVER_ERROR);
            }

    }

}
